#include "dr/FlexDR.h"
#include <algorithm>

using namespace std;
using namespace fr;






void FlexDRWorker::regionDensity(){

    auto area_calc = [](vector<frBlockObject*>& objects){
        long total_area = 0;
        for(auto obj : objects){
            if (obj->typeId() == frcInstTerm) {
                auto instTerm = static_cast<frInstTerm*>(obj);        
                
                frBox inst_box;
                instTerm->getInst()->getBBox(inst_box);
                long long area = inst_box.length() * inst_box.width();
                total_area += area;
            }//end if
        }//end for 
        return total_area;
    };

    


    // std::cout << "feature extraction regionDensity ... \n";
    vector<frBlockObject*> instance_objects_route_box;
    vector<frBlockObject*> instance_objects_ext_box;
    vector<frBlockObject*> instance_objects_drc_box;
    
    double route_box_area = getRouteBox().length() * getRouteBox().width();
    double drc_box_area = getDrcBox().length() * getDrcBox().width();
    double ext_box_area = getExtBox().length() * getExtBox().width();



    getRegionQuery()->queryGRPin(getRouteBox(), instance_objects_route_box);
    getRegionQuery()->queryGRPin(getRouteBox(), instance_objects_ext_box);
    getRegionQuery()->queryGRPin(getRouteBox(), instance_objects_drc_box);

    

    long long instances_total_area_route_box = area_calc(instance_objects_route_box);
    long long instances_total_area_ext_box = area_calc(instance_objects_ext_box);
    long long instances_total_area_drc_box = area_calc(instance_objects_drc_box);


    double region_density_route_box = instances_total_area_route_box/route_box_area;
    double region_density_ext_box = instances_total_area_ext_box/ext_box_area;
    double region_density_drc_box = instances_total_area_drc_box/drc_box_area;


    
    // std::cout << "region_density_route_box: " << region_density_route_box << std::endl;
    // std::cout << "region_density_ext_box: "   << region_density_ext_box   << std::endl;
    // std::cout << "region_density_drc_box: "   << region_density_drc_box   << std::endl;

    // std::cout << "instance_objects_route_box: " << instance_objects_route_box.size() << std::endl;
    // std::cout << "instance_objects_ext_box: "   << instance_objects_ext_box.size()   << std::endl;
    // std::cout << "instance_objects_drc_box: "   << instance_objects_drc_box.size()   << std::endl;

    

    m_feature_extraction.m_region_density_route_box = region_density_route_box;
    m_feature_extraction.m_region_density_ext_box   = region_density_ext_box;
    m_feature_extraction.m_region_density_drc_box   = region_density_drc_box;


    m_feature_extraction.m_route_box = getRouteBox();
    m_feature_extraction.m_ext_box = getExtBox();
    m_feature_extraction.m_drc_box = getDrcBox();

    m_feature_extraction.m_number_of_cells_route_box = instance_objects_route_box.size();
    m_feature_extraction.m_number_of_cells_ext_box = instance_objects_ext_box.size();
    m_feature_extraction.m_number_of_cells_drc_box = instance_objects_drc_box.size();


    for(auto& inst : instance_objects_route_box)
    {
        m_feature_extraction.m_instances.push_back(inst);
    }
        

    for(auto obj : instance_objects_route_box){
        if (obj->typeId() == frcInstTerm) {
            auto instTerm = static_cast<frInstTerm*>(obj);        
            m_feature_extraction.m_instances_name.insert(instTerm->getInst()->getName());
            m_feature_extraction.m_instances_type.insert(instTerm->getTerm()->getName());
        }//end if
    }//end for 

    // std::cout << "gr_pin_objects size: " << gr_pin_objects.size() << std::endl;
    // for(auto obj : gr_pin_objects){
    //     if (obj->typeId() == frcInstTerm) {
    //         auto instTerm = static_cast<frInstTerm*>(obj);
    //         std::cout << "inst: " << instTerm->getInst()->getName() << std::endl;
    //         frBox inst_box;
    //         instTerm->getInst()->getBBox(inst_box);
    //         std::cout << "area: " << inst_box.length() * inst_box.width() << std::endl;
    //     }
        
    // }//end result for


    // auto& layers = getTech()->getLayers();
    // std::cout << "layers: " << layers.size() << std::endl;

    // for(auto& layer : layers){
    //     frRegionQuery::Objects<frBlockObject> objects;
    //     std::cout << "layer: " << layer->getLayerNum() << std::endl;
    //     getRegionQuery()->query(getRouteBox(),layer->getLayerNum(),objects);
    //     std::cout << "objects size: " << objects.size() << std::endl;
    //     for (auto &[bx, rqObj]: objects) {
    //         if (rqObj->typeId() == frcInstTerm) {
    //             auto instTerm = static_cast<frInstTerm*>(rqObj);
    //             cout <<"    found " <<instTerm->getInst()->getName() <<"/" <<instTerm->getTerm()->getName() <<endl;
    //         }//end if 
    //     }//end for 
    // }//end for 
    
    // auto& layers = getTech()->getLayers();
    // auto layer = layers[1]->getLayerNum();
    // getRegionQuery()->queryDRObj(getRouteBox(), dr_objects);
    // getRegionQuery()->queryGRPin(getRouteBox(), gr_pin_objects);
    // getRegionQuery()->queryGuide(getRouteBox(),guide_objects);
    // getRegionQuery()->queryMarker(getRouteBox(),marker_objects);
    // getRegionQuery()->queryGuide(getRouteBox(),guide_objects);
    // // 
    // std::cout << "marker_objects size: " << marker_objects.size() << std::endl;
    // for(auto block : marker_objects){
    //     std::cout << "type id: " << block->typeId() << std::endl;
    // }//end result for

    // std::cout << "guide_objects size: " << guide_objects.size() << std::endl;
    // for(auto block : guide_objects){
    //     std::cout << "type id: " << block->typeId() << std::endl;
    // }//end result for

    // std::cout << "gr_pin_objects size: " << gr_pin_objects.size() << std::endl;
    // for(auto block : gr_pin_objects){
    //     std::cout << "type id: " << block->typeId() << std::endl;
    // }//end result for

    // frPoint bp, ep;
    // std::cout << "dr_objects size: " << dr_objects.size() << std::endl;
    // for(auto block : dr_objects){
    //     std::cout << "type id: " << block->typeId() << std::endl;
    //     if(block->typeId() == frcPathSeg){
    //       auto obj = static_cast<frPathSeg*>(block);
    //       obj->getPoints(bp, ep);
    //       auto lNum = obj->getLayerNum();
    //       frCoord psLen = ep.x() - bp.x() + ep.y() - bp.y();
    //       std::cout << "wl: " << psLen << std::endl;
    //     }//end if 
    // }//end result for


    // // calculate the area of worker
    // frBox route_box = getRouteBox();
    // std::cout << "##################################Report##########################\n";
    // std::cout << "route box: " 
    //           << route_box.lowerLeft().x()  << ", " 
    //           << route_box.lowerLeft().y()  << ", "
    //           << route_box.upperRight().x() << ", "
    //           << route_box.upperRight().y() << std::endl;

    // std::cout << "route box area: " << route_box.length() * route_box.width() << std::endl;




    



    // std::cout << "##################################End Report##########################\n";

}//end regionDensity


 void FlexDRWorker::macroDensity(){
    auto& layers = getTech()->getLayers();


    for(auto& layer : layers){
        frRegionQuery::Objects<frBlockObject> objects;
        getRegionQuery()->query(getRouteBox(),layer->getLayerNum(),objects);
        std::cout << "objects size: " << objects.size() << std::endl;
        for (auto &[bx, rqObj]: objects) {
            if (rqObj->typeId() == frcInstTerm) {
                auto instTerm = static_cast<frInstTerm*>(rqObj);
                const auto& blk_ref = instTerm->getInst()->getRefBlock();
                cout <<"found " << instTerm->getInst()->getName() <<"/" << blk_ref->getName() << "/"<< instTerm->getTerm()->getName() <<endl;
            }//end if 
        }//end for 
    }//end for 

 }//end macroDensity 


 void FlexDRWorker::rotationCells(){
     
     auto rotate = [](vector<frBlockObject*>& objects){
        long total_area = 0;
        for(auto obj : objects){
            if (obj->typeId() == frcInstTerm) {
                auto instTerm = static_cast<frInstTerm*>(obj);        
                
                frBox inst_box;
                if(instTerm->getInst()->getName() == "inst8879"){
                    frOrient new_orient = frcMY;
                    instTerm->getInst()->setOrient(new_orient);
                }
            }//end if
        }//end for 
        return total_area;
    };

    


    // std::cout << "feature extraction regionDensity ... \n";
    vector<frBlockObject*> instance_objects_route_box;


    getRegionQuery()->queryGRPin(getRouteBox(), instance_objects_route_box);


    long long instances_total_area_route_box = rotate(instance_objects_route_box);



 }//end rotationCell 

 void FlexDRWorker::routingDensity(){
    
    auto area_calc = [](vector<frBlockObject*>& objects){
        long total_area = 0;
        for(auto obj : objects){
            if (obj->typeId() == frcInstTerm) {
                auto instTerm = static_cast<frInstTerm*>(obj);        
                
                frBox inst_box;
                instTerm->getInst()->getBBox(inst_box);
                long long area = inst_box.length() * inst_box.width();
                total_area += area;
            }//end if
        }//end for 
        return total_area;
    };

    


    // std::cout << "feature extraction regionDensity ... \n";
    vector<frBlockObject*> dr_objects;

    getRegionQuery()->queryDRObj(getRouteBox(),dr_objects);

    int m_number_of_wires;
    int m_number_of_patches;
    int m_number_of_vias;
    long long m_wl_of_worker;
   
    frPoint bp, ep;
    // std::cout << "dr_objects size: " << dr_objects.size() << std::endl;
    for(auto block : dr_objects){
        // std::cout << "type id: " << block->typeId() << std::endl;
        if(block->typeId() == frcPathSeg){
          m_feature_extraction.m_number_of_wires +=1;
          auto obj = static_cast<frPathSeg*>(block);
          auto net = obj->getNet();
          frBox wire_box;
          obj->getBBox(wire_box);
        //   std::cout << "net name: " << net->getName() << std::endl;
        //   std::cout << "wire box: " << wire_box.lowerLeft().x() << ", "
        //                             << wire_box.lowerLeft().y() << ", "
        //                             << wire_box.upperRight().x() << ", "
        //                             << wire_box.upperRight().y() << "\n";
                                    

          obj->getPoints(bp, ep);
          auto lNum = obj->getLayerNum();
          frCoord psLen = std::abs(ep.x() - bp.x()) + std::abs(ep.y() - bp.y());
        //   std::cout << "wl: " << psLen << std::endl;
           m_feature_extraction.m_wl_of_worker += psLen;
        }else if(block->typeId() == frcVia){
            m_feature_extraction.m_number_of_vias +=1;
        }else if(block->typeId() == frcPatchWire){
            m_feature_extraction.m_number_of_patches +=1;
        }//end if 
    }//end result for

//      cout <<endl <<"total wire length = " <<totWlen / getDesign()->getTopBlock()->getDBUPerUU() <<" um" <<endl;
    // for (int i = getTech()->getBottomLayerNum(); i <= getTech()->getTopLayerNum(); i++) {
    //   if (getTech()->getLayer(i)->getType() == frLayerTypeEnum::ROUTING) {
    //     cout <<"total wire length on LAYER " <<getTech()->getLayer(i)->getName() <<" = " 
    //          <<wlen[i] / getDesign()->getTopBlock()->getDBUPerUU() <<" um" <<endl;
    //   }
    // }
    // cout <<"total number of vias = " <<totSCut + totMCut <<endl;
    // if (totMCut > 0) {
    //   cout <<"total number of multi-cut vias = " <<totMCut 
    //        << " (" <<setw(5) <<fixed <<setprecision(1) <<totMCut * 100.0 / (totSCut + totMCut) <<"%)" <<endl;
    //   cout <<"total number of single-cut vias = " <<totSCut 
    //        << " (" <<setw(5) <<fixed <<setprecision(1) <<totSCut * 100.0 / (totSCut + totMCut) <<"%)" <<endl;
    // }
    // cout <<"up-via summary (total " <<totSCut + totMCut <<"):" <<endl;
    // int nameLen = 0;
    // for (int i = getTech()->getBottomLayerNum(); i <= getTech()->getTopLayerNum(); i++) {
    //   if (getTech()->getLayer(i)->getType() == frLayerTypeEnum::CUT) {
    //     nameLen = max(nameLen, (int)getTech()->getLayer(i-1)->getName().size());
    //   }
    // }
    // int maxL = 1 + nameLen + 4 + (int)to_string(totSCut).length();
    // if (totMCut) {
    //   maxL += 9 + 4 + (int)to_string(totMCut).length() + 9 + 4 + (int)to_string(totSCut + totMCut).length();
    // }
    // if (totMCut) {
    //   cout <<" " <<setw(nameLen + 4 + (int)to_string(totSCut).length() + 9) <<"single-cut";
    //   cout <<setw(4 + (int)to_string(totMCut).length() + 9) <<"multi-cut" 
    //        <<setw(4 + (int)to_string(totSCut + totMCut).length()) <<"total";
    // }
    // cout <<endl;
    // for (int i = 0; i < maxL; i++) {
    //   cout <<"-";
    // }
    // cout <<endl;
    // for (int i = getTech()->getBottomLayerNum(); i <= getTech()->getTopLayerNum(); i++) {
    //   if (getTech()->getLayer(i)->getType() == frLayerTypeEnum::CUT) {
    //     cout <<" "    <<setw(nameLen) <<getTech()->getLayer(i-1)->getName() 
    //          <<"    " <<setw((int)to_string(totSCut).length()) <<sCut[i];
    //     if (totMCut) {
    //       cout <<" ("   <<setw(5) <<(double)((sCut[i] + mCut[i]) ? sCut[i] * 100.0 / (sCut[i] + mCut[i]) : 0.0) <<"%)";
    //       cout <<"    " <<setw((int)to_string(totMCut).length()) <<mCut[i] 
    //            <<" ("   <<setw(5) <<(double)((sCut[i] + mCut[i]) ? mCut[i] * 100.0 / (sCut[i] + mCut[i]) : 0.0) <<"%)"
    //            <<"    " <<setw((int)to_string(totSCut + totMCut).length()) <<sCut[i] + mCut[i];
    //     }
    //     cout <<endl;
    //   }
    // }
    // for (int i = 0; i < maxL; i++) {
    //   cout <<"-";
    // }
    // cout <<endl;
    // cout <<" "    <<setw(nameLen) <<""
    //      <<"    " <<setw((int)to_string(totSCut).length()) <<totSCut;
    // if (totMCut) {
    //   cout <<" ("   <<setw(5) <<(double)((totSCut + totMCut) ? totSCut * 100.0 / (totSCut + totMCut) : 0.0) <<"%)";
    //   cout <<"    " <<setw((int)to_string(totMCut).length()) <<totMCut 
    //        <<" ("   <<setw(5) <<(double)((totSCut + totMCut) ? totMCut * 100.0 / (totSCut + totMCut) : 0.0) <<"%)"
    //        <<"    " <<setw((int)to_string(totSCut + totMCut).length()) <<totSCut + totMCut;
    // }
    // cout <<endl <<endl <<flush;
    // guard.restore();

 }//end routingDensity


