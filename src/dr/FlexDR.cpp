/* Authors: Lutong Wang and Bangqi Xu */
/*
 * Copyright (c) 2019, The Regents of the University of California
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <chrono>
#include <fstream>
#include <boost/io/ios_state.hpp>
//#include <taskflow/taskflow.hpp>
#include "dr/FlexDR.h"
#include "io/io.h"
#include "db/infra/frTime.h"

#include <omp.h>
#include <unordered_map>

#include "pa/FlexPA.h"
#include <boost/foreach.hpp>



using namespace std;
using namespace fr;



int FlexDRWorker::main() {
  using namespace std::chrono;
  high_resolution_clock::time_point t0 = high_resolution_clock::now();
  if (VERBOSE > 1) {
    frBox scaledBox;
    stringstream ss;
    ss <<endl <<"start DR worker (BOX) "
                <<"( " <<routeBox.left()   * 1.0 / getTech()->getDBUPerUU() <<" "
                <<routeBox.bottom() * 1.0 / getTech()->getDBUPerUU() <<" ) ( "
                <<routeBox.right()  * 1.0 / getTech()->getDBUPerUU() <<" "
                <<routeBox.top()    * 1.0 / getTech()->getDBUPerUU() <<" )" <<endl;
    cout <<ss.str() <<flush;
  }

  init();
  high_resolution_clock::time_point t1 = high_resolution_clock::now();
  // std::cout << "before route\n";
  // featureExtraction();
  route();
  high_resolution_clock::time_point t2 = high_resolution_clock::now();
  end();
  high_resolution_clock::time_point t3 = high_resolution_clock::now();

  duration<double> time_span0 = duration_cast<duration<double>>(t1 - t0);
  duration<double> time_span1 = duration_cast<duration<double>>(t2 - t1);
  duration<double> time_span2 = duration_cast<duration<double>>(t3 - t2);

  if (VERBOSE > 1) {
    stringstream ss;
    ss   <<"time (INIT/ROUTE/POST) " <<time_span0.count() <<" " 
                                     <<time_span1.count() <<" "
                                     <<time_span2.count() <<" "
                                     <<endl;
    cout <<ss.str() <<flush;
  }
  return 0;
}

void FlexDRWorker::featureExtraction(){
    std::cout << "feature extaction inside DRWorker ...\n";

    vector<frBlockObject*> dr_objects;
    vector<frBlockObject*> gr_pin_objects;
    vector<frGuide*> guide_objects;
    vector<frMarker*> marker_objects;
    frRegionQuery::Objects<frBlockObject> objects;
    // auto& layers = getTech()->getLayers();
    // auto layer = layers[1]->getLayerNum();
    getRegionQuery()->queryDRObj(getRouteBox(), dr_objects);
    getRegionQuery()->queryGRPin(getRouteBox(), gr_pin_objects);
    getRegionQuery()->queryGuide(getRouteBox(),guide_objects);
    getRegionQuery()->queryMarker(getRouteBox(),marker_objects);
    getRegionQuery()->queryGuide(getRouteBox(),guide_objects);

    auto& layers = getTech()->getLayers();
   
      // getRegionQuery()->query(getRouteBox(),layer,objects);
    std::cout << "marker_objects size: " << marker_objects.size() << std::endl;
    for(auto block : marker_objects){
        std::cout << "type id: " << block->typeId() << std::endl;
    }//end result for

    std::cout << "guide_objects size: " << guide_objects.size() << std::endl;
    for(auto block : guide_objects){
        std::cout << "type id: " << block->typeId() << std::endl;
    }//end result for

    std::cout << "gr_pin_objects size: " << gr_pin_objects.size() << std::endl;
    for(auto block : gr_pin_objects){
        std::cout << "type id: " << block->typeId() << std::endl;
    }//end result for

    frPoint bp, ep;
    std::cout << "dr_objects size: " << dr_objects.size() << std::endl;
    for(auto block : dr_objects){
        std::cout << "type id: " << block->typeId() << std::endl;
        if(block->typeId() == frcPathSeg){
          auto obj = static_cast<frPathSeg*>(block);
          obj->getPoints(bp, ep);
          auto lNum = obj->getLayerNum();
          frCoord psLen = ep.x() - bp.x() + ep.y() - bp.y();
          std::cout << "wl: " << psLen << std::endl;
        }//end if 
    }//end result for


    // calculate the area of worker
    frBox route_box = getRouteBox();
    std::cout << "##################################Report##########################\n";
    std::cout << "route box: " 
              << route_box.lowerLeft().x()  << ", " 
              << route_box.lowerLeft().y()  << ", "
              << route_box.upperRight().x() << ", "
              << route_box.upperRight().y() << std::endl;

    std::cout << "route box area: " << route_box.length() * route_box.width() << std::endl;




    



    std::cout << "##################################End Report##########################\n";


    
    



    // vector<unsigned long long> wlen(getTech()->getLayers().size(), 0);
    // vector<unsigned long long> sCut(getTech()->getLayers().size(), 0);
    // vector<unsigned long long> mCut(getTech()->getLayers().size(), 0);
    // unsigned long long totWlen = 0;
    // unsigned long long totSCut = 0;
    // unsigned long long totMCut = 0;
    // frPoint bp, ep;
    // for (auto &net: getDesign()->getTopBlock()->getNets()) {
    //   for (auto &shape: net->getShapes()) {
    //     if (shape->typeId() == frcPathSeg) {
    //       auto obj = static_cast<frPathSeg*>(shape.get());
    //       obj->getPoints(bp, ep);
    //       auto lNum = obj->getLayerNum();
    //       frCoord psLen = ep.x() - bp.x() + ep.y() - bp.y();
    //       wlen[lNum] += psLen;
    //       totWlen += psLen;
    //     }
    //   }
    //   for (auto &via: net->getVias()) {
    //     auto lNum = via->getViaDef()->getCutLayerNum();
    //     if (via->getViaDef()->isMultiCut()) {
    //       ++mCut[lNum];
    //       ++totMCut;
    //     } else {
    //       ++sCut[lNum];
    //       ++totSCut;
    //     }
    //   }
    // }

    return;

    for(auto& net : nets){
      std::cout << net->getFrNet()->getName() << std::endl;
      std::cout << "get original guides: " << net->getFrNet()->getGuides().size() << std::endl;
      for (auto &guide: net->getFrNet()->getGuides()) {
        frBox guide_box;
        guide->getBBox(guide_box);
        auto begin_layer = guide->getBeginLayerNum();
        auto end_layer = guide->getEndLayerNum();
        
        std::cout << "guide: " << guide_box.lowerLeft().x() << ", "
                               << guide_box.lowerLeft().y() << ", "
                               << guide_box.upperRight().x() << ", "
                               << guide_box.upperRight().y() << std::endl;
        std::cout << "begin layer: " << begin_layer << std::endl;
        std::cout << "end layer: " << end_layer << std::endl;
      }//end for net->getGuides()


      //wires 
      for(auto& shape : net->getFrNet()->getShapes()){
        frBox wire_shape;
        shape->getBBox(wire_shape);
        auto layer_name = shape->getLayerNum();
                
        std::cout << "wire: "  << wire_shape.lowerLeft().x() << ", "
                               << wire_shape.lowerLeft().y() << ", "
                               << wire_shape.upperRight().x() << ", "
                               << wire_shape.upperRight().y() << ", "
                               << layer_name << std::endl;
                              
      }//end for net->getFrNet()->getShapes()

      for(auto& via : net->getFrNet()->getVias()){
        auto layerName = getTech()->getLayer(via->getViaDef()->getLayer1Num())->getName();
        auto viaName = via->getViaDef()->getName();
        frPoint origin;
        via->getOrigin(origin);
        std::cout << layerName.c_str() << ", " << 
                origin.x() << ", " << origin.y() << ", " << viaName.c_str() << std::endl;
      }//end net->getFrNet()->getVias()


      // print patch wires 
      for(auto& wire : net->getFrNet()->getPatchWires()){
          auto layerName = getTech()->getLayer(wire->getLayerNum())->getName();
          frBox wire_box;
          wire->getBBox(wire_box);
          std::cout << "pwire: " << wire_box.lowerLeft().x() << ", "
                                << wire_box.lowerLeft().y() << ", "
                                << wire_box.upperRight().x() << ", "
                                << wire_box.upperRight().y() << ", "
                                << layerName <<std::endl;
      }//end net->getFrNet()->getPatchWires()
    }//end for nets

}//end featureExtraction

int FlexDRWorker::main_mt() {
  using namespace std::chrono;
  high_resolution_clock::time_point t0 = high_resolution_clock::now();
  if (VERBOSE > 1) {
    frBox scaledBox;
    stringstream ss;
    ss <<endl <<"start DR worker (BOX) "
                <<"( " <<routeBox.left()   * 1.0 / getTech()->getDBUPerUU() <<" "
                <<routeBox.bottom() * 1.0 / getTech()->getDBUPerUU() <<" ) ( "
                <<routeBox.right()  * 1.0 / getTech()->getDBUPerUU() <<" "
                <<routeBox.top()    * 1.0 / getTech()->getDBUPerUU() <<" )" <<endl;
    cout <<ss.str() <<flush;
  }

  init();
  high_resolution_clock::time_point t1 = high_resolution_clock::now();
  if (getFixMode() != 9) {
    std::cout << "route erfan\n";
    route();
  } else {
    std::cout << "route queue erfan\n";
   route_queue();
  }
  high_resolution_clock::time_point t2 = high_resolution_clock::now();
  cleanup();
  high_resolution_clock::time_point t3 = high_resolution_clock::now();

  duration<double> time_span0 = duration_cast<duration<double>>(t1 - t0);
  duration<double> time_span1 = duration_cast<duration<double>>(t2 - t1);
  duration<double> time_span2 = duration_cast<duration<double>>(t3 - t2);

  if (VERBOSE > 1) {
    stringstream ss;
    ss   <<"time (INIT/ROUTE/POST) " <<time_span0.count() <<" " 
                                     <<time_span1.count() <<" "
                                     <<time_span2.count() <<" "
                                     <<endl;
    cout <<ss.str() <<flush;
  }
  return 0;
} //end main_mt

int FlexDRWorker::main_mt_feature_extraction() {
  using namespace std::chrono;
  high_resolution_clock::time_point t0 = high_resolution_clock::now();
  if (VERBOSE > 1) {
    frBox scaledBox;
    stringstream ss;
    ss <<endl <<"start DR worker (BOX) "
                <<"( " <<routeBox.left()   * 1.0 / getTech()->getDBUPerUU() <<" "
                <<routeBox.bottom() * 1.0 / getTech()->getDBUPerUU() <<" ) ( "
                <<routeBox.right()  * 1.0 / getTech()->getDBUPerUU() <<" "
                <<routeBox.top()    * 1.0 / getTech()->getDBUPerUU() <<" )" <<endl;
    cout <<ss.str() <<flush;
  }

  init();
  high_resolution_clock::time_point t1 = high_resolution_clock::now();
  std::cout << "start feature extracting ...\n";
  regionDensity();
  macroDensity();
  routingDensity();
  // rotationCells();
  // if (getFixMode() != 9) {
  //   std::cout << "route erfan\n";
  //   route();
  // } else {
  //   std::cout << "route queue erfan\n";
  //  route_queue();
  // }
  high_resolution_clock::time_point t2 = high_resolution_clock::now();
  cleanup();
  high_resolution_clock::time_point t3 = high_resolution_clock::now();

  duration<double> time_span0 = duration_cast<duration<double>>(t1 - t0);
  duration<double> time_span1 = duration_cast<duration<double>>(t2 - t1);
  duration<double> time_span2 = duration_cast<duration<double>>(t3 - t2);

  if (VERBOSE > 1) {
    stringstream ss;
    ss   <<"time (INIT/ROUTE/POST) " <<time_span0.count() <<" " 
                                     <<time_span1.count() <<" "
                                     <<time_span2.count() <<" "
                                     <<endl;
    cout <<ss.str() <<flush;
  }
  return 0;
}//end main_mt_feature_extraction

void FlexDR::initFromTA() {
  bool enableOutput = false;
  // initialize lists
  for (auto &net: getDesign()->getTopBlock()->getNets()) {
    for (auto &guide: net->getGuides()) {
      for (auto &connFig: guide->getRoutes()) {
        if (connFig->typeId() == frcPathSeg) {
          unique_ptr<frShape> ps = make_unique<frPathSeg>(*(static_cast<frPathSeg*>(connFig.get())));
          frPoint bp, ep;
          static_cast<frPathSeg*>(ps.get())->getPoints(bp, ep);
          if (ep.x() - bp.x() + ep.y() - bp.y() == 1) {
            ; // skip TA dummy segment
          } else {
            net->addShape(std::move(ps));
          }
        } else {
          cout <<"Error: initFromTA unsupported shape" <<endl;
        }
      }
    }
  }

  if (enableOutput) {
    for (auto &net: getDesign()->getTopBlock()->getNets()) {
      cout <<"net " <<net->getName() <<" has " <<net->getShapes().size() <<" shape" <<endl;
    }
  }
}

void FlexDR::initGCell2BoundaryPin() {
  bool enableOutput = false;
  // initiailize size
  frBox dieBox;
  getDesign()->getTopBlock()->getBoundaryBBox(dieBox);
  auto gCellPatterns = getDesign()->getTopBlock()->getGCellPatterns();
  auto &xgp = gCellPatterns.at(0);
  auto &ygp = gCellPatterns.at(1);
  auto tmpVec = vector<map<frNet*, set<pair<frPoint, frLayerNum> >, frBlockObjectComp> >((int)ygp.getCount());
  gcell2BoundaryPin = vector<vector<map<frNet*, set<pair<frPoint, frLayerNum> >, frBlockObjectComp> > >((int)xgp.getCount(), tmpVec);
  for (auto &net: getDesign()->getTopBlock()->getNets()) {
    auto netPtr = net.get();
    for (auto &guide: net->getGuides()) {
      for (auto &connFig: guide->getRoutes()) {
        if (connFig->typeId() == frcPathSeg) {
          auto ps = static_cast<frPathSeg*>(connFig.get());
          frLayerNum layerNum;
          frPoint bp, ep;
          ps->getPoints(bp, ep);
          layerNum = ps->getLayerNum();
          // skip TA dummy segment
          if (ep.x() - bp.x() + ep.y() - bp.y() == 1 || ep.x() - bp.x() + ep.y() - bp.y() == 0) {
            continue; 
          }
          frPoint idx1, idx2;
          getDesign()->getTopBlock()->getGCellIdx(bp, idx1);
          getDesign()->getTopBlock()->getGCellIdx(ep, idx2);
          // update gcell2BoundaryPin
          // horizontal
          if (bp.y() == ep.y()) {
            int x1 = idx1.x();
            int x2 = idx2.x();
            int y  = idx1.y();
            for (auto x = x1; x <= x2; ++x) {
              frBox gcellBox;
              getDesign()->getTopBlock()->getGCellBox(frPoint(x, y), gcellBox);
              frCoord leftBound  = gcellBox.left();
              frCoord rightBound = gcellBox.right();
              bool hasLeftBound  = true;
              bool hasRightBound = true;
              if (bp.x() < leftBound) {
                hasLeftBound = true;
              } else {
                hasLeftBound = false;
              }
              if (ep.x() >= rightBound) {
                hasRightBound = true;
              } else {
                hasRightBound = false;
              }
              if (hasLeftBound) {
                frPoint boundaryPt(leftBound, bp.y());
                gcell2BoundaryPin[x][y][netPtr].insert(make_pair(boundaryPt, layerNum));
                if (enableOutput) {
                  cout << "init left boundary pin (" 
                       << boundaryPt.x() * 1.0 / getDesign()->getTopBlock()->getDBUPerUU() << ", " 
                       << boundaryPt.y() * 1.0 / getDesign()->getTopBlock()->getDBUPerUU() << ") at ("
                       << x <<", " <<y <<") " 
                       << getTech()->getLayer(layerNum)->getName() <<" "
                       << string((net == nullptr) ? "null" : net->getName()) <<"\n";
                }
              }
              if (hasRightBound) {
                frPoint boundaryPt(rightBound, ep.y());
                gcell2BoundaryPin[x][y][netPtr].insert(make_pair(boundaryPt, layerNum));
                if (enableOutput) {
                  cout << "init right boundary pin (" 
                       << boundaryPt.x() * 1.0 / getDesign()->getTopBlock()->getDBUPerUU() << ", " 
                       << boundaryPt.y() * 1.0 / getDesign()->getTopBlock()->getDBUPerUU() << ") at ("
                       << x <<", " <<y <<") " 
                       << getTech()->getLayer(layerNum)->getName() <<" "
                       << string((net == nullptr) ? "null" : net->getName()) <<"\n";
                }
              }
            }
          } else if (bp.x() == ep.x()) {
            int x  = idx1.x();
            int y1 = idx1.y();
            int y2 = idx2.y();
            for (auto y = y1; y <= y2; ++y) {
              frBox gcellBox;
              getDesign()->getTopBlock()->getGCellBox(frPoint(x, y), gcellBox);
              frCoord bottomBound = gcellBox.bottom();
              frCoord topBound    = gcellBox.top();
              bool hasBottomBound = true;
              bool hasTopBound    = true;
              if (bp.y() < bottomBound) {
                hasBottomBound = true;
              } else {
                hasBottomBound = false;
              }
              if (ep.y() >= topBound) {
                hasTopBound = true;
              } else {
                hasTopBound = false;
              }
              if (hasBottomBound) {
                frPoint boundaryPt(bp.x(), bottomBound);
                gcell2BoundaryPin[x][y][netPtr].insert(make_pair(boundaryPt, layerNum));
                if (enableOutput) {
                  cout << "init bottom boundary pin (" 
                       << boundaryPt.x() * 1.0 / getDesign()->getTopBlock()->getDBUPerUU() << ", " 
                       << boundaryPt.y() * 1.0 / getDesign()->getTopBlock()->getDBUPerUU() << ") at ("
                       << x <<", " <<y <<") " 
                       << getTech()->getLayer(layerNum)->getName() <<" "
                       << string((net == nullptr) ? "null" : net->getName()) <<"\n";
                }
              }
              if (hasTopBound) {
                frPoint boundaryPt(ep.x(), topBound);
                gcell2BoundaryPin[x][y][netPtr].insert(make_pair(boundaryPt, layerNum));
                if (enableOutput) {
                  cout << "init top boundary pin (" 
                       << boundaryPt.x() * 1.0 / getDesign()->getTopBlock()->getDBUPerUU() << ", " 
                       << boundaryPt.y() * 1.0 / getDesign()->getTopBlock()->getDBUPerUU() << ") at ("
                       << x <<", " <<y <<") " 
                       << getTech()->getLayer(layerNum)->getName() <<" "
                       << string((net == nullptr) ? "null" : net->getName()) <<"\n";
                }
              }
            }
          } else {
            cout << "Error: non-orthogonal pathseg in initGCell2BoundryPin\n";
          }
        }
      }
    }
  }
}

frCoord FlexDR::init_via2viaMinLen_minimumcut1(frLayerNum lNum, frViaDef* viaDef1, frViaDef* viaDef2) {
  if (!(viaDef1 && viaDef2)) {
    return 0;
  }

  frCoord sol = 0;

  // check min len in lNum assuming pre dir routing
  bool isH = (getDesign()->getTech()->getLayer(lNum)->getDir() == frPrefRoutingDirEnum::frcHorzPrefRoutingDir);

  bool isVia1Above = false;
  frVia via1(viaDef1);
  frBox viaBox1, cutBox1;
  if (viaDef1->getLayer1Num() == lNum) {
    via1.getLayer1BBox(viaBox1);
    isVia1Above = true;
  } else {
    via1.getLayer2BBox(viaBox1);
    isVia1Above = false;
  }
  via1.getCutBBox(cutBox1);
  auto width1    = viaBox1.width();
  auto length1   = viaBox1.length();

  bool isVia2Above = false;
  frVia via2(viaDef2);
  frBox viaBox2, cutBox2;
  if (viaDef2->getLayer1Num() == lNum) {
    via2.getLayer1BBox(viaBox2);
    isVia2Above = true;
  } else {
    via2.getLayer2BBox(viaBox2);
    isVia2Above = false;
  }
  via2.getCutBBox(cutBox2);
  auto width2    = viaBox2.width();
  auto length2   = viaBox2.length();

  for (auto &con: getDesign()->getTech()->getLayer(lNum)->getMinimumcutConstraints()) {
    if ((!con->hasLength() || (con->hasLength() && length1 > con->getLength())) && 
        width1 > con->getWidth()) {
      bool checkVia2 = false;
      if (!con->hasConnection()) {
        checkVia2 = true;
      } else {
        if (con->getConnection() == frMinimumcutConnectionEnum::FROMABOVE && isVia2Above) {
          checkVia2 = true;
        } else if (con->getConnection() == frMinimumcutConnectionEnum::FROMBELOW && !isVia2Above) {
          checkVia2 = true;
        }
      }
      if (!checkVia2) {
        continue;
      }
      if (isH) {
        sol = max(sol, (con->hasLength() ? con->getDistance() : 0) + 
                       max(cutBox2.right() - 0 + 0 - viaBox1.left(), 
                           viaBox1.right() - 0 + 0 - cutBox2.left()));
      } else {
        sol = max(sol, (con->hasLength() ? con->getDistance() : 0) + 
                       max(cutBox2.top() - 0 + 0 - viaBox1.bottom(), 
                           viaBox1.top() - 0 + 0 - cutBox2.bottom()));
      }
    } 
    // check via1cut to via2metal
    if ((!con->hasLength() || (con->hasLength() && length2 > con->getLength())) && 
        width2 > con->getWidth()) {
      bool checkVia1 = false;
      if (!con->hasConnection()) {
        checkVia1 = true;
      } else {
        if (con->getConnection() == frMinimumcutConnectionEnum::FROMABOVE && isVia1Above) {
          checkVia1 = true;
        } else if (con->getConnection() == frMinimumcutConnectionEnum::FROMBELOW && !isVia1Above) {
          checkVia1 = true;
        }
      }
      if (!checkVia1) {
        continue;
      }
      if (isH) {
        sol = max(sol, (con->hasLength() ? con->getDistance() : 0) + 
                       max(cutBox1.right() - 0 + 0 - viaBox2.left(), 
                           viaBox2.right() - 0 + 0 - cutBox1.left()));
      } else {
        sol = max(sol, (con->hasLength() ? con->getDistance() : 0) + 
                       max(cutBox1.top() - 0 + 0 - viaBox2.bottom(), 
                           viaBox2.top() - 0 + 0 - cutBox1.bottom()));
      }
    }
  }

  return sol;
}

bool FlexDR::init_via2viaMinLen_minimumcut2(frLayerNum lNum, frViaDef* viaDef1, frViaDef* viaDef2) {
  if (!(viaDef1 && viaDef2)) {
    return true;
  }
  // skip if same-layer via
  if (viaDef1 == viaDef2) {
    return true;
  }

  bool sol = true;

  bool isVia1Above = false;
  frVia via1(viaDef1);
  frBox viaBox1, cutBox1;
  if (viaDef1->getLayer1Num() == lNum) {
    via1.getLayer1BBox(viaBox1);
    isVia1Above = true;
  } else {
    via1.getLayer2BBox(viaBox1);
    isVia1Above = false;
  }
  via1.getCutBBox(cutBox1);
  auto width1    = viaBox1.width();

  bool isVia2Above = false;
  frVia via2(viaDef2);
  frBox viaBox2, cutBox2;
  if (viaDef2->getLayer1Num() == lNum) {
    via2.getLayer1BBox(viaBox2);
    isVia2Above = true;
  } else {
    via2.getLayer2BBox(viaBox2);
    isVia2Above = false;
  }
  via2.getCutBBox(cutBox2);
  auto width2    = viaBox2.width();

  for (auto &con: getDesign()->getTech()->getLayer(lNum)->getMinimumcutConstraints()) {
    if (con->hasLength()) {
      continue;
    }
    // check via2cut to via1metal
    if (width1 > con->getWidth()) {
      bool checkVia2 = false;
      if (!con->hasConnection()) {
        checkVia2 = true;
      } else {
        // has length rule
        if (con->getConnection() == frMinimumcutConnectionEnum::FROMABOVE && isVia2Above) {
          checkVia2 = true;
        } else if (con->getConnection() == frMinimumcutConnectionEnum::FROMBELOW && !isVia2Above) {
          checkVia2 = true;
        }
      }
      if (!checkVia2) {
        continue;
      }
      sol = false;
      break;
    } 
    // check via1cut to via2metal
    if (width2 > con->getWidth()) {
      bool checkVia1 = false;
      if (!con->hasConnection()) {
        checkVia1 = true;
      } else {
        if (con->getConnection() == frMinimumcutConnectionEnum::FROMABOVE && isVia1Above) {
          checkVia1 = true;
        } else if (con->getConnection() == frMinimumcutConnectionEnum::FROMBELOW && !isVia1Above) {
          checkVia1 = true;
        }
      }
      if (!checkVia1) {
        continue;
      }
      sol = false;
      break;
    }
  }
  return sol;
}

frCoord FlexDR::init_via2viaMinLen_minSpc(frLayerNum lNum, frViaDef* viaDef1, frViaDef* viaDef2) {
  if (!(viaDef1 && viaDef2)) {
    //cout <<"hehehehehe" <<endl;
    return 0;
  }

  frCoord sol = 0;

  // check min len in lNum assuming pre dir routing
  bool isH = (getDesign()->getTech()->getLayer(lNum)->getDir() == frPrefRoutingDirEnum::frcHorzPrefRoutingDir);
  frCoord defaultWidth = getDesign()->getTech()->getLayer(lNum)->getWidth();

  frVia via1(viaDef1);
  frBox viaBox1;
  if (viaDef1->getLayer1Num() == lNum) {
    via1.getLayer1BBox(viaBox1);
  } else {
    via1.getLayer2BBox(viaBox1);
  }
  auto width1    = viaBox1.width();
  bool isVia1Fat = isH ? (viaBox1.top() - viaBox1.bottom() > defaultWidth) : (viaBox1.right() - viaBox1.left() > defaultWidth);
  auto prl1      = isH ? (viaBox1.top() - viaBox1.bottom()) : (viaBox1.right() - viaBox1.left());

  frVia via2(viaDef2);
  frBox viaBox2;
  if (viaDef2->getLayer1Num() == lNum) {
    via2.getLayer1BBox(viaBox2);
  } else {
    via2.getLayer2BBox(viaBox2);
  }
  auto width2    = viaBox2.width();
  bool isVia2Fat = isH ? (viaBox2.top() - viaBox2.bottom() > defaultWidth) : (viaBox2.right() - viaBox2.left() > defaultWidth);
  auto prl2      = isH ? (viaBox2.top() - viaBox2.bottom()) : (viaBox2.right() - viaBox2.left());

  frCoord reqDist = 0;
  if (isVia1Fat && isVia2Fat) {
    auto con = getDesign()->getTech()->getLayer(lNum)->getMinSpacing();
    if (con) {
      if (con->typeId() == frConstraintTypeEnum::frcSpacingConstraint) {
        reqDist = static_cast<frSpacingConstraint*>(con)->getMinSpacing();
      } else if (con->typeId() == frConstraintTypeEnum::frcSpacingTablePrlConstraint) {
        reqDist = static_cast<frSpacingTablePrlConstraint*>(con)->find(max(width1, width2), min(prl1, prl2));
      } else if (con->typeId() == frConstraintTypeEnum::frcSpacingTableTwConstraint) {
        reqDist = static_cast<frSpacingTableTwConstraint*>(con)->find(width1, width2, min(prl1, prl2));
      }
    }
    if (isH) {
      reqDist += max((viaBox1.right() - 0), (0 - viaBox1.left()));
      reqDist += max((viaBox2.right() - 0), (0 - viaBox2.left()));
    } else {
      reqDist += max((viaBox1.top() - 0), (0 - viaBox1.bottom()));
      reqDist += max((viaBox2.top() - 0), (0 - viaBox2.bottom()));
    }
    sol = max(sol, reqDist);
  }

  // check min len in layer2 if two vias are in same layer
  if (viaDef1 != viaDef2) {
    return sol;
  }

  if (viaDef1->getLayer1Num() == lNum) {
    via1.getLayer2BBox(viaBox1);
    lNum = lNum + 2;
  } else {
    via1.getLayer1BBox(viaBox1);
    lNum = lNum - 2;
  }
  width1    = viaBox1.width();
  prl1      = isH ? (viaBox1.top() - viaBox1.bottom()) : (viaBox1.right() - viaBox1.left());
  reqDist   = 0;
  auto con = getDesign()->getTech()->getLayer(lNum)->getMinSpacing();
  if (con) {
    if (con->typeId() == frConstraintTypeEnum::frcSpacingConstraint) {
      reqDist = static_cast<frSpacingConstraint*>(con)->getMinSpacing();
    } else if (con->typeId() == frConstraintTypeEnum::frcSpacingTablePrlConstraint) {
      reqDist = static_cast<frSpacingTablePrlConstraint*>(con)->find(max(width1, width2), prl1);
    } else if (con->typeId() == frConstraintTypeEnum::frcSpacingTableTwConstraint) {
      reqDist = static_cast<frSpacingTableTwConstraint*>(con)->find(width1, width2, prl1);
    }
  }
  if (isH) {
    reqDist += (viaBox1.right() - 0) + (0 - viaBox1.left());
  } else {
    reqDist += (viaBox1.top() - 0) + (0 - viaBox1.bottom());
  }
  sol = max(sol, reqDist);
  
  return sol;
}

void FlexDR::init_via2viaMinLen() {
  bool enableOutput = false;
  // bool enableOutput = true;
  auto bottomLayerNum = getDesign()->getTech()->getBottomLayerNum();
  auto topLayerNum    = getDesign()->getTech()->getTopLayerNum();
  for (auto lNum = bottomLayerNum; lNum <= topLayerNum; lNum++) {
    if (getDesign()->getTech()->getLayer(lNum)->getType() != frLayerTypeEnum::ROUTING) {
      continue;
    }
    vector<frCoord> via2viaMinLenTmp(4, 0);
    vector<bool>    via2viaZeroLen(4, true);
    via2viaMinLen.push_back(make_pair(via2viaMinLenTmp, via2viaZeroLen));
  }
  // check prl
  int i = 0;
  for (auto lNum = bottomLayerNum; lNum <= topLayerNum; lNum++) {
    if (getDesign()->getTech()->getLayer(lNum)->getType() != frLayerTypeEnum::ROUTING) {
      continue;
    }
    frViaDef* downVia = nullptr;
    frViaDef* upVia   = nullptr;
    if (getDesign()->getTech()->getBottomLayerNum() <= lNum - 1) {
      downVia = getDesign()->getTech()->getLayer(lNum - 1)->getDefaultViaDef();
    }
    if (getDesign()->getTech()->getTopLayerNum() >= lNum + 1) {
      upVia = getDesign()->getTech()->getLayer(lNum + 1)->getDefaultViaDef();
    }
    (via2viaMinLen[i].first)[0] = max((via2viaMinLen[i].first)[0], init_via2viaMinLen_minSpc(lNum, downVia, downVia));
    (via2viaMinLen[i].first)[1] = max((via2viaMinLen[i].first)[1], init_via2viaMinLen_minSpc(lNum, downVia, upVia));
    (via2viaMinLen[i].first)[2] = max((via2viaMinLen[i].first)[2], init_via2viaMinLen_minSpc(lNum, upVia, downVia));
    (via2viaMinLen[i].first)[3] = max((via2viaMinLen[i].first)[3], init_via2viaMinLen_minSpc(lNum, upVia, upVia));
    if (enableOutput) {
      cout <<"initVia2ViaMinLen_minSpc " <<getDesign()->getTech()->getLayer(lNum)->getName()
           <<" (d2d, d2u, u2d, u2u) = (" 
           <<(via2viaMinLen[i].first)[0] <<", "
           <<(via2viaMinLen[i].first)[1] <<", "
           <<(via2viaMinLen[i].first)[2] <<", "
           <<(via2viaMinLen[i].first)[3] <<")" <<endl;
    }
    i++;
  }

  // check minimumcut
  i = 0;
  for (auto lNum = bottomLayerNum; lNum <= topLayerNum; lNum++) {
    if (getDesign()->getTech()->getLayer(lNum)->getType() != frLayerTypeEnum::ROUTING) {
      continue;
    }
    frViaDef* downVia = nullptr;
    frViaDef* upVia   = nullptr;
    if (getDesign()->getTech()->getBottomLayerNum() <= lNum - 1) {
      downVia = getDesign()->getTech()->getLayer(lNum - 1)->getDefaultViaDef();
    }
    if (getDesign()->getTech()->getTopLayerNum() >= lNum + 1) {
      upVia = getDesign()->getTech()->getLayer(lNum + 1)->getDefaultViaDef();
    }
    vector<frCoord> via2viaMinLenTmp(4, 0);
    (via2viaMinLen[i].first)[0] = max((via2viaMinLen[i].first)[0], init_via2viaMinLen_minimumcut1(lNum, downVia, downVia));
    (via2viaMinLen[i].first)[1] = max((via2viaMinLen[i].first)[1], init_via2viaMinLen_minimumcut1(lNum, downVia, upVia));
    (via2viaMinLen[i].first)[2] = max((via2viaMinLen[i].first)[2], init_via2viaMinLen_minimumcut1(lNum, upVia, downVia));
    (via2viaMinLen[i].first)[3] = max((via2viaMinLen[i].first)[3], init_via2viaMinLen_minimumcut1(lNum, upVia, upVia));
    (via2viaMinLen[i].second)[0] = (via2viaMinLen[i].second)[0] && init_via2viaMinLen_minimumcut2(lNum, downVia, downVia);
    (via2viaMinLen[i].second)[1] = (via2viaMinLen[i].second)[1] && init_via2viaMinLen_minimumcut2(lNum, downVia, upVia);
    (via2viaMinLen[i].second)[2] = (via2viaMinLen[i].second)[2] && init_via2viaMinLen_minimumcut2(lNum, upVia, downVia);
    (via2viaMinLen[i].second)[3] = (via2viaMinLen[i].second)[3] && init_via2viaMinLen_minimumcut2(lNum, upVia, upVia);
    if (enableOutput) {
      cout <<"initVia2ViaMinLen_minimumcut " <<getDesign()->getTech()->getLayer(lNum)->getName()
           <<" (d2d, d2u, u2d, u2u) = (" 
           <<(via2viaMinLen[i].first)[0] <<", "
           <<(via2viaMinLen[i].first)[1] <<", "
           <<(via2viaMinLen[i].first)[2] <<", "
           <<(via2viaMinLen[i].first)[3] <<")" <<endl;
      cout <<"initVia2ViaMinLen_minimumcut " <<getDesign()->getTech()->getLayer(lNum)->getName()
           <<" zerolen (b, b, b, b) = (" 
           <<(via2viaMinLen[i].second)[0] <<", "
           <<(via2viaMinLen[i].second)[1] <<", "
           <<(via2viaMinLen[i].second)[2] <<", "
           <<(via2viaMinLen[i].second)[3] <<")" 
           <<endl;
    }
    i++;
  }
}

frCoord FlexDR::init_via2viaMinLenNew_minimumcut1(frLayerNum lNum, frViaDef* viaDef1, frViaDef* viaDef2, bool isCurrDirY) {
  if (!(viaDef1 && viaDef2)) {
    return 0;
  }

  frCoord sol = 0;

  // check min len in lNum assuming pre dir routing
  bool isCurrDirX = !isCurrDirY;

  bool isVia1Above = false;
  frVia via1(viaDef1);
  frBox viaBox1, cutBox1;
  if (viaDef1->getLayer1Num() == lNum) {
    via1.getLayer1BBox(viaBox1);
    isVia1Above = true;
  } else {
    via1.getLayer2BBox(viaBox1);
    isVia1Above = false;
  }
  via1.getCutBBox(cutBox1);
  auto width1    = viaBox1.width();
  auto length1   = viaBox1.length();

  bool isVia2Above = false;
  frVia via2(viaDef2);
  frBox viaBox2, cutBox2;
  if (viaDef2->getLayer1Num() == lNum) {
    via2.getLayer1BBox(viaBox2);
    isVia2Above = true;
  } else {
    via2.getLayer2BBox(viaBox2);
    isVia2Above = false;
  }
  via2.getCutBBox(cutBox2);
  auto width2    = viaBox2.width();
  auto length2   = viaBox2.length();

  for (auto &con: getDesign()->getTech()->getLayer(lNum)->getMinimumcutConstraints()) {
    // check via2cut to via1metal
    // no length OR metal1 shape satisfies --> check via2
    if ((!con->hasLength() || (con->hasLength() && length1 > con->getLength())) && 
        width1 > con->getWidth()) {
      bool checkVia2 = false;
      if (!con->hasConnection()) {
        checkVia2 = true;
      } else {
        if (con->getConnection() == frMinimumcutConnectionEnum::FROMABOVE && isVia2Above) {
          checkVia2 = true;
        } else if (con->getConnection() == frMinimumcutConnectionEnum::FROMBELOW && !isVia2Above) {
          checkVia2 = true;
        }
      }
      if (!checkVia2) {
        continue;
      }
      if (isCurrDirX) {
        sol = max(sol, (con->hasLength() ? con->getDistance() : 0) + 
                       max(cutBox2.right() - 0 + 0 - viaBox1.left(), 
                           viaBox1.right() - 0 + 0 - cutBox2.left()));
      } else {
        sol = max(sol, (con->hasLength() ? con->getDistance() : 0) + 
                       max(cutBox2.top() - 0 + 0 - viaBox1.bottom(), 
                           viaBox1.top() - 0 + 0 - cutBox2.bottom()));
      }
    } 
    // check via1cut to via2metal
    if ((!con->hasLength() || (con->hasLength() && length2 > con->getLength())) && 
        width2 > con->getWidth()) {
      bool checkVia1 = false;
      if (!con->hasConnection()) {
        checkVia1 = true;
      } else {
        if (con->getConnection() == frMinimumcutConnectionEnum::FROMABOVE && isVia1Above) {
          checkVia1 = true;
        } else if (con->getConnection() == frMinimumcutConnectionEnum::FROMBELOW && !isVia1Above) {
          checkVia1 = true;
        }
      }
      if (!checkVia1) {
        continue;
      }
      if (isCurrDirX) {
        sol = max(sol, (con->hasLength() ? con->getDistance() : 0) + 
                       max(cutBox1.right() - 0 + 0 - viaBox2.left(), 
                           viaBox2.right() - 0 + 0 - cutBox1.left()));
      } else {
        sol = max(sol, (con->hasLength() ? con->getDistance() : 0) + 
                       max(cutBox1.top() - 0 + 0 - viaBox2.bottom(), 
                           viaBox2.top() - 0 + 0 - cutBox1.bottom()));
      }
    }
  }

  return sol;
}

frCoord FlexDR::init_via2viaMinLenNew_minSpc(frLayerNum lNum, frViaDef* viaDef1, frViaDef* viaDef2, bool isCurrDirY) {
  if (!(viaDef1 && viaDef2)) {
    return 0;
  }

  frCoord sol = 0;

  // check min len in lNum assuming pre dir routing
  bool isCurrDirX = !isCurrDirY;
  frCoord defaultWidth = getDesign()->getTech()->getLayer(lNum)->getWidth();

  frVia via1(viaDef1);
  frBox viaBox1;
  if (viaDef1->getLayer1Num() == lNum) {
    via1.getLayer1BBox(viaBox1);
  } else {
    via1.getLayer2BBox(viaBox1);
  }
  auto width1    = viaBox1.width();
  bool isVia1Fat = isCurrDirX ? (viaBox1.top() - viaBox1.bottom() > defaultWidth) : (viaBox1.right() - viaBox1.left() > defaultWidth);
  auto prl1      = isCurrDirX ? (viaBox1.top() - viaBox1.bottom()) : (viaBox1.right() - viaBox1.left());

  frVia via2(viaDef2);
  frBox viaBox2;
  if (viaDef2->getLayer1Num() == lNum) {
    via2.getLayer1BBox(viaBox2);
  } else {
    via2.getLayer2BBox(viaBox2);
  }
  auto width2    = viaBox2.width();
  bool isVia2Fat = isCurrDirX ? (viaBox2.top() - viaBox2.bottom() > defaultWidth) : (viaBox2.right() - viaBox2.left() > defaultWidth);
  auto prl2      = isCurrDirX ? (viaBox2.top() - viaBox2.bottom()) : (viaBox2.right() - viaBox2.left());

  frCoord reqDist = 0;
  if (isVia1Fat && isVia2Fat) {
    auto con = getDesign()->getTech()->getLayer(lNum)->getMinSpacing();
    if (con) {
      if (con->typeId() == frConstraintTypeEnum::frcSpacingConstraint) {
        reqDist = static_cast<frSpacingConstraint*>(con)->getMinSpacing();
      } else if (con->typeId() == frConstraintTypeEnum::frcSpacingTablePrlConstraint) {
        reqDist = static_cast<frSpacingTablePrlConstraint*>(con)->find(max(width1, width2), min(prl1, prl2));
      } else if (con->typeId() == frConstraintTypeEnum::frcSpacingTableTwConstraint) {
        reqDist = static_cast<frSpacingTableTwConstraint*>(con)->find(width1, width2, min(prl1, prl2));
      }
    }
    if (isCurrDirX) {
      reqDist += max((viaBox1.right() - 0), (0 - viaBox1.left()));
      reqDist += max((viaBox2.right() - 0), (0 - viaBox2.left()));
    } else {
      reqDist += max((viaBox1.top() - 0), (0 - viaBox1.bottom()));
      reqDist += max((viaBox2.top() - 0), (0 - viaBox2.bottom()));
    }
    sol = max(sol, reqDist);
  }

  // check min len in layer2 if two vias are in same layer
  if (viaDef1 != viaDef2) {
    return sol;
  }

  if (viaDef1->getLayer1Num() == lNum) {
    via1.getLayer2BBox(viaBox1);
    lNum = lNum + 2;
  } else {
    via1.getLayer1BBox(viaBox1);
    lNum = lNum - 2;
  }
  width1    = viaBox1.width();
  prl1      = isCurrDirX ? (viaBox1.top() - viaBox1.bottom()) : (viaBox1.right() - viaBox1.left());
  reqDist   = 0;
  auto con = getDesign()->getTech()->getLayer(lNum)->getMinSpacing();
  if (con) {
    if (con->typeId() == frConstraintTypeEnum::frcSpacingConstraint) {
      reqDist = static_cast<frSpacingConstraint*>(con)->getMinSpacing();
    } else if (con->typeId() == frConstraintTypeEnum::frcSpacingTablePrlConstraint) {
      reqDist = static_cast<frSpacingTablePrlConstraint*>(con)->find(max(width1, width2), prl1);
    } else if (con->typeId() == frConstraintTypeEnum::frcSpacingTableTwConstraint) {
      reqDist = static_cast<frSpacingTableTwConstraint*>(con)->find(width1, width2, prl1);
    }
  }
  if (isCurrDirX) {
    reqDist += (viaBox1.right() - 0) + (0 - viaBox1.left());
  } else {
    reqDist += (viaBox1.top() - 0) + (0 - viaBox1.bottom());
  }
  sol = max(sol, reqDist);
  
  return sol;
}

frCoord FlexDR::init_via2viaMinLenNew_cutSpc(frLayerNum lNum, frViaDef* viaDef1, frViaDef* viaDef2, bool isCurrDirY) {
  if (!(viaDef1 && viaDef2)) {
    return 0;
  }

  frCoord sol = 0;

  // check min len in lNum assuming pre dir routing
  frVia via1(viaDef1);
  frBox viaBox1, cutBox1;
  if (viaDef1->getLayer1Num() == lNum) {
    via1.getLayer1BBox(viaBox1);
  } else {
    via1.getLayer2BBox(viaBox1);
  }
  via1.getCutBBox(cutBox1);

  frVia via2(viaDef2);
  frBox viaBox2, cutBox2;
  if (viaDef2->getLayer1Num() == lNum) {
    via2.getLayer1BBox(viaBox2);
  } else {
    via2.getLayer2BBox(viaBox2);
  }
  via2.getCutBBox(cutBox2);

  // same layer (use samenet rule if exist, otherwise use diffnet rule)
  if (viaDef1->getCutLayerNum() == viaDef2->getCutLayerNum()) {
    auto samenetCons = getDesign()->getTech()->getLayer(viaDef1->getCutLayerNum())->getCutSpacing(true);
    auto diffnetCons = getDesign()->getTech()->getLayer(viaDef1->getCutLayerNum())->getCutSpacing(false);
    if (!samenetCons.empty()) {
      // check samenet spacing rule if exists
      for (auto con: samenetCons) {
        if (con == nullptr) {
          continue;
        }
        // filter rule, assuming default via will never trigger cutArea
        if (con->hasSecondLayer() || con->isAdjacentCuts() || con->isParallelOverlap() || con->isArea() || !con->hasSameNet()) {
          continue;
        }
        auto reqSpcVal = con->getCutSpacing();
        if (!con->hasCenterToCenter()) {
          reqSpcVal += isCurrDirY ? (cutBox1.top() - cutBox1.bottom()) : (cutBox1.right() - cutBox1.left());
        }
        sol = max(sol, reqSpcVal);
      }
    } else {
      // check diffnet spacing rule
      // filter rule, assuming default via will never trigger cutArea
      for (auto con: diffnetCons) {
        if (con == nullptr) {
          continue;
        }
        if (con->hasSecondLayer() || con->isAdjacentCuts() || con->isParallelOverlap() || con->isArea() || con->hasSameNet()) {
          continue;
        }
        auto reqSpcVal = con->getCutSpacing();
        if (!con->hasCenterToCenter()) {
          reqSpcVal += isCurrDirY ? (cutBox1.top() - cutBox1.bottom()) : (cutBox1.right() - cutBox1.left());
        }
        sol = max(sol, reqSpcVal);
      }
    }
  // TODO: diff layer 
  } else {
    auto layerNum1 = viaDef1->getCutLayerNum();
    auto layerNum2 = viaDef2->getCutLayerNum();
    frCutSpacingConstraint* samenetCon = nullptr;
    if (getDesign()->getTech()->getLayer(layerNum1)->hasInterLayerCutSpacing(layerNum2, true)) {
      samenetCon = getDesign()->getTech()->getLayer(layerNum1)->getInterLayerCutSpacing(layerNum2, true);
    }
    if (getDesign()->getTech()->getLayer(layerNum2)->hasInterLayerCutSpacing(layerNum1, true)) {
      if (samenetCon) {
        cout <<"Warning: duplicate diff layer samenet cut spacing, skipping cut spacing from " 
             <<layerNum2 <<" to " <<layerNum1 <<endl;
      } else {
        samenetCon = getDesign()->getTech()->getLayer(layerNum2)->getInterLayerCutSpacing(layerNum1, true);
      }
    }
    if (samenetCon == nullptr) {
      if (getDesign()->getTech()->getLayer(layerNum1)->hasInterLayerCutSpacing(layerNum2, false)) {
        samenetCon = getDesign()->getTech()->getLayer(layerNum1)->getInterLayerCutSpacing(layerNum2, false);
      }
      if (getDesign()->getTech()->getLayer(layerNum2)->hasInterLayerCutSpacing(layerNum1, false)) {
        if (samenetCon) {
          cout <<"Warning: duplicate diff layer diffnet cut spacing, skipping cut spacing from " 
               <<layerNum2 <<" to " <<layerNum1 <<endl;
        } else {
          samenetCon = getDesign()->getTech()->getLayer(layerNum2)->getInterLayerCutSpacing(layerNum1, false);
        }
      }
    }
    if (samenetCon) {
      // filter rule, assuming default via will never trigger cutArea
      auto reqSpcVal = samenetCon->getCutSpacing();
      if (reqSpcVal == 0) {
        ;
      } else {
        if (!samenetCon->hasCenterToCenter()) {
          reqSpcVal += isCurrDirY ? (cutBox1.top() - cutBox1.bottom()) : (cutBox1.right() - cutBox1.left());
        }
      }
      sol = max(sol, reqSpcVal);
    }
  }

  return sol;
}

void FlexDR::init_via2viaMinLenNew() {
  bool enableOutput = false;
  // bool enableOutput = true;
  auto bottomLayerNum = getDesign()->getTech()->getBottomLayerNum();
  auto topLayerNum    = getDesign()->getTech()->getTopLayerNum();
  for (auto lNum = bottomLayerNum; lNum <= topLayerNum; lNum++) {
    if (getDesign()->getTech()->getLayer(lNum)->getType() != frLayerTypeEnum::ROUTING) {
      continue;
    }
    vector<frCoord> via2viaMinLenTmp(8, 0);
    via2viaMinLenNew.push_back(via2viaMinLenTmp);
  }
  // check prl
  int i = 0;
  for (auto lNum = bottomLayerNum; lNum <= topLayerNum; lNum++) {
    if (getDesign()->getTech()->getLayer(lNum)->getType() != frLayerTypeEnum::ROUTING) {
      continue;
    }
    frViaDef* downVia = nullptr;
    frViaDef* upVia   = nullptr;
    if (getDesign()->getTech()->getBottomLayerNum() <= lNum - 1) {
      downVia = getDesign()->getTech()->getLayer(lNum - 1)->getDefaultViaDef();
    }
    if (getDesign()->getTech()->getTopLayerNum() >= lNum + 1) {
      upVia = getDesign()->getTech()->getLayer(lNum + 1)->getDefaultViaDef();
    }
    via2viaMinLenNew[i][0] = max(via2viaMinLenNew[i][0], init_via2viaMinLenNew_minSpc(lNum, downVia, downVia, false));
    via2viaMinLenNew[i][1] = max(via2viaMinLenNew[i][1], init_via2viaMinLenNew_minSpc(lNum, downVia, downVia, true ));
    via2viaMinLenNew[i][2] = max(via2viaMinLenNew[i][2], init_via2viaMinLenNew_minSpc(lNum, downVia, upVia,   false));
    via2viaMinLenNew[i][3] = max(via2viaMinLenNew[i][3], init_via2viaMinLenNew_minSpc(lNum, downVia, upVia,   true ));
    via2viaMinLenNew[i][4] = max(via2viaMinLenNew[i][4], init_via2viaMinLenNew_minSpc(lNum, upVia,   downVia, false));
    via2viaMinLenNew[i][5] = max(via2viaMinLenNew[i][5], init_via2viaMinLenNew_minSpc(lNum, upVia,   downVia, true ));
    via2viaMinLenNew[i][6] = max(via2viaMinLenNew[i][6], init_via2viaMinLenNew_minSpc(lNum, upVia,   upVia,   false));
    via2viaMinLenNew[i][7] = max(via2viaMinLenNew[i][7], init_via2viaMinLenNew_minSpc(lNum, upVia,   upVia,   true ));
    if (enableOutput) {
      cout <<"initVia2ViaMinLenNew_minSpc " <<getDesign()->getTech()->getLayer(lNum)->getName()
           <<" (d2d-x, d2d-y, d2u-x, d2u-y, u2d-x, u2d-y, u2u-x, u2u-y) = (" 
           <<via2viaMinLenNew[i][0] <<", "
           <<via2viaMinLenNew[i][1] <<", "
           <<via2viaMinLenNew[i][2] <<", "
           <<via2viaMinLenNew[i][3] <<", "
           <<via2viaMinLenNew[i][4] <<", "
           <<via2viaMinLenNew[i][5] <<", "
           <<via2viaMinLenNew[i][6] <<", "
           <<via2viaMinLenNew[i][7] <<")" <<endl;
    }
    i++;
  }

  // check minimumcut
  i = 0;
  for (auto lNum = bottomLayerNum; lNum <= topLayerNum; lNum++) {
    if (getDesign()->getTech()->getLayer(lNum)->getType() != frLayerTypeEnum::ROUTING) {
      continue;
    }
    frViaDef* downVia = nullptr;
    frViaDef* upVia   = nullptr;
    if (getDesign()->getTech()->getBottomLayerNum() <= lNum - 1) {
      downVia = getDesign()->getTech()->getLayer(lNum - 1)->getDefaultViaDef();
    }
    if (getDesign()->getTech()->getTopLayerNum() >= lNum + 1) {
      upVia = getDesign()->getTech()->getLayer(lNum + 1)->getDefaultViaDef();
    }
    via2viaMinLenNew[i][0] = max(via2viaMinLenNew[i][0], init_via2viaMinLenNew_minimumcut1(lNum, downVia, downVia, false));
    via2viaMinLenNew[i][1] = max(via2viaMinLenNew[i][1], init_via2viaMinLenNew_minimumcut1(lNum, downVia, downVia, true ));
    via2viaMinLenNew[i][2] = max(via2viaMinLenNew[i][2], init_via2viaMinLenNew_minimumcut1(lNum, downVia, upVia,   false));
    via2viaMinLenNew[i][3] = max(via2viaMinLenNew[i][3], init_via2viaMinLenNew_minimumcut1(lNum, downVia, upVia,   true ));
    via2viaMinLenNew[i][4] = max(via2viaMinLenNew[i][4], init_via2viaMinLenNew_minimumcut1(lNum, upVia,   downVia, false));
    via2viaMinLenNew[i][5] = max(via2viaMinLenNew[i][5], init_via2viaMinLenNew_minimumcut1(lNum, upVia,   downVia, true ));
    via2viaMinLenNew[i][6] = max(via2viaMinLenNew[i][6], init_via2viaMinLenNew_minimumcut1(lNum, upVia,   upVia,   false));
    via2viaMinLenNew[i][7] = max(via2viaMinLenNew[i][7], init_via2viaMinLenNew_minimumcut1(lNum, upVia,   upVia,   true ));
    if (enableOutput) {
      cout <<"initVia2ViaMinLenNew_minimumcut " <<getDesign()->getTech()->getLayer(lNum)->getName()
           <<" (d2d-x, d2d-y, d2u-x, d2u-y, u2d-x, u2d-y, u2u-x, u2u-y) = (" 
           <<via2viaMinLenNew[i][0] <<", "
           <<via2viaMinLenNew[i][1] <<", "
           <<via2viaMinLenNew[i][2] <<", "
           <<via2viaMinLenNew[i][3] <<", "
           <<via2viaMinLenNew[i][4] <<", "
           <<via2viaMinLenNew[i][5] <<", "
           <<via2viaMinLenNew[i][6] <<", "
           <<via2viaMinLenNew[i][7] <<")" <<endl;
    }
    i++;
  }

  // check cut spacing
  i = 0;
  for (auto lNum = bottomLayerNum; lNum <= topLayerNum; lNum++) {
    if (getDesign()->getTech()->getLayer(lNum)->getType() != frLayerTypeEnum::ROUTING) {
      continue;
    }
    frViaDef* downVia = nullptr;
    frViaDef* upVia   = nullptr;
    if (getDesign()->getTech()->getBottomLayerNum() <= lNum - 1) {
      downVia = getDesign()->getTech()->getLayer(lNum - 1)->getDefaultViaDef();
    }
    if (getDesign()->getTech()->getTopLayerNum() >= lNum + 1) {
      upVia = getDesign()->getTech()->getLayer(lNum + 1)->getDefaultViaDef();
    }
    via2viaMinLenNew[i][0] = max(via2viaMinLenNew[i][0], init_via2viaMinLenNew_cutSpc(lNum, downVia, downVia, false));
    via2viaMinLenNew[i][1] = max(via2viaMinLenNew[i][1], init_via2viaMinLenNew_cutSpc(lNum, downVia, downVia, true ));
    via2viaMinLenNew[i][2] = max(via2viaMinLenNew[i][2], init_via2viaMinLenNew_cutSpc(lNum, downVia, upVia,   false));
    via2viaMinLenNew[i][3] = max(via2viaMinLenNew[i][3], init_via2viaMinLenNew_cutSpc(lNum, downVia, upVia,   true ));
    via2viaMinLenNew[i][4] = max(via2viaMinLenNew[i][4], init_via2viaMinLenNew_cutSpc(lNum, upVia,   downVia, false));
    via2viaMinLenNew[i][5] = max(via2viaMinLenNew[i][5], init_via2viaMinLenNew_cutSpc(lNum, upVia,   downVia, true ));
    via2viaMinLenNew[i][6] = max(via2viaMinLenNew[i][6], init_via2viaMinLenNew_cutSpc(lNum, upVia,   upVia,   false));
    via2viaMinLenNew[i][7] = max(via2viaMinLenNew[i][7], init_via2viaMinLenNew_cutSpc(lNum, upVia,   upVia,   true ));
    if (enableOutput) {
      cout <<"initVia2ViaMinLenNew_cutSpc " <<getDesign()->getTech()->getLayer(lNum)->getName()
           <<" (d2d-x, d2d-y, d2u-x, d2u-y, u2d-x, u2d-y, u2u-x, u2u-y) = (" 
           <<via2viaMinLenNew[i][0] <<", "
           <<via2viaMinLenNew[i][1] <<", "
           <<via2viaMinLenNew[i][2] <<", "
           <<via2viaMinLenNew[i][3] <<", "
           <<via2viaMinLenNew[i][4] <<", "
           <<via2viaMinLenNew[i][5] <<", "
           <<via2viaMinLenNew[i][6] <<", "
           <<via2viaMinLenNew[i][7] <<")" <<endl;
    }
    i++;
  }

}

void FlexDR::init_halfViaEncArea() {
  auto bottomLayerNum = getDesign()->getTech()->getBottomLayerNum();
  auto topLayerNum    = getDesign()->getTech()->getTopLayerNum();
  for (int i = bottomLayerNum; i <= topLayerNum; i++) {
    if (getDesign()->getTech()->getLayer(i)->getType() != frLayerTypeEnum::ROUTING) {
      continue;
    }
    if (i + 1 <= topLayerNum && getDesign()->getTech()->getLayer(i + 1)->getType() == frLayerTypeEnum::CUT) {
      auto viaDef = getTech()->getLayer(i + 1)->getDefaultViaDef();
      frVia via(viaDef);
      frBox layer1Box;
      frBox layer2Box;
      via.getLayer1BBox(layer1Box);
      via.getLayer2BBox(layer2Box);
      auto layer1HalfArea = layer1Box.width() * layer1Box.length() / 2;
      auto layer2HalfArea = layer2Box.width() * layer2Box.length() / 2;
      halfViaEncArea.push_back(make_pair(layer1HalfArea, layer2HalfArea));
    } else {
      halfViaEncArea.push_back(make_pair(0,0));
    }
  }
}



frCoord FlexDR::init_via2turnMinLen_minSpc(frLayerNum lNum, frViaDef* viaDef, bool isCurrDirY) {
  if (!viaDef) {
    return 0;
  }

  frCoord sol = 0;

  // check min len in lNum assuming pre dir routing
  bool isCurrDirX = !isCurrDirY;
  frCoord defaultWidth = getDesign()->getTech()->getLayer(lNum)->getWidth();

  frVia via1(viaDef);
  frBox viaBox1;
  if (viaDef->getLayer1Num() == lNum) {
    via1.getLayer1BBox(viaBox1);
  } else {
    via1.getLayer2BBox(viaBox1);
  }
  auto width1    = viaBox1.width();
  bool isVia1Fat = isCurrDirX ? (viaBox1.top() - viaBox1.bottom() > defaultWidth) : (viaBox1.right() - viaBox1.left() > defaultWidth);
  auto prl1      = isCurrDirX ? (viaBox1.top() - viaBox1.bottom()) : (viaBox1.right() - viaBox1.left());

  frCoord reqDist = 0;
  if (isVia1Fat) {
    auto con = getDesign()->getTech()->getLayer(lNum)->getMinSpacing();
    if (con) {
      if (con->typeId() == frConstraintTypeEnum::frcSpacingConstraint) {
        reqDist = static_cast<frSpacingConstraint*>(con)->getMinSpacing();
      } else if (con->typeId() == frConstraintTypeEnum::frcSpacingTablePrlConstraint) {
        reqDist = static_cast<frSpacingTablePrlConstraint*>(con)->find(max(width1, defaultWidth), prl1);
      } else if (con->typeId() == frConstraintTypeEnum::frcSpacingTableTwConstraint) {
        reqDist = static_cast<frSpacingTableTwConstraint*>(con)->find(width1, defaultWidth, prl1);
      }
    }
    if (isCurrDirX) {
      reqDist += max((viaBox1.right() - 0), (0 - viaBox1.left()));
      reqDist += defaultWidth;
    } else {
      reqDist += max((viaBox1.top() - 0), (0 - viaBox1.bottom()));
      reqDist += defaultWidth;
    }
    sol = max(sol, reqDist);
  }

  return sol;
}

frCoord FlexDR::init_via2turnMinLen_minStp(frLayerNum lNum, frViaDef* viaDef, bool isCurrDirY) {
  if (!viaDef) {
    return 0;
  }

  frCoord sol = 0;

  // check min len in lNum assuming pre dir routing
  bool isCurrDirX = !isCurrDirY;
  frCoord defaultWidth = getDesign()->getTech()->getLayer(lNum)->getWidth();

  frVia via1(viaDef);
  frBox viaBox1;
  if (viaDef->getLayer1Num() == lNum) {
    via1.getLayer1BBox(viaBox1);
  } else {
    via1.getLayer2BBox(viaBox1);
  }
  bool isVia1Fat = isCurrDirX ? (viaBox1.top() - viaBox1.bottom() > defaultWidth) : (viaBox1.right() - viaBox1.left() > defaultWidth);

  frCoord reqDist = 0;
  if (isVia1Fat) {
    auto con = getDesign()->getTech()->getLayer(lNum)->getMinStepConstraint();
    if (con && con->hasMaxEdges()) { // currently only consider maxedge violation
      reqDist = con->getMinStepLength();
      if (isCurrDirX) {
        reqDist += max((viaBox1.right() - 0), (0 - viaBox1.left()));
        reqDist += defaultWidth;
      } else {
        reqDist += max((viaBox1.top() - 0), (0 - viaBox1.bottom()));
        reqDist += defaultWidth;
      }
      sol = max(sol, reqDist);
    }
  }
  return sol;
}


void FlexDR::init_via2turnMinLen() {
  bool enableOutput = false;
  //bool enableOutput = true;
  auto bottomLayerNum = getDesign()->getTech()->getBottomLayerNum();
  auto topLayerNum    = getDesign()->getTech()->getTopLayerNum();
  for (auto lNum = bottomLayerNum; lNum <= topLayerNum; lNum++) {
    if (getDesign()->getTech()->getLayer(lNum)->getType() != frLayerTypeEnum::ROUTING) {
      continue;
    }
    vector<frCoord> via2turnMinLenTmp(4, 0);
    via2turnMinLen.push_back(via2turnMinLenTmp);
  }
  // check prl
  int i = 0;
  for (auto lNum = bottomLayerNum; lNum <= topLayerNum; lNum++) {
    if (getDesign()->getTech()->getLayer(lNum)->getType() != frLayerTypeEnum::ROUTING) {
      continue;
    }
    frViaDef* downVia = nullptr;
    frViaDef* upVia   = nullptr;
    if (getDesign()->getTech()->getBottomLayerNum() <= lNum - 1) {
      downVia = getDesign()->getTech()->getLayer(lNum - 1)->getDefaultViaDef();
    }
    if (getDesign()->getTech()->getTopLayerNum() >= lNum + 1) {
      upVia = getDesign()->getTech()->getLayer(lNum + 1)->getDefaultViaDef();
    }
    via2turnMinLen[i][0] = max(via2turnMinLen[i][0], init_via2turnMinLen_minSpc(lNum, downVia, false));
    via2turnMinLen[i][1] = max(via2turnMinLen[i][1], init_via2turnMinLen_minSpc(lNum, downVia, true));
    via2turnMinLen[i][2] = max(via2turnMinLen[i][2], init_via2turnMinLen_minSpc(lNum, upVia, false));
    via2turnMinLen[i][3] = max(via2turnMinLen[i][3], init_via2turnMinLen_minSpc(lNum, upVia, true));
    if (enableOutput) {
      cout <<"initVia2TurnMinLen_minSpc " <<getDesign()->getTech()->getLayer(lNum)->getName()
           <<" (down->x->y, down->y->x, up->x->y, up->y->x) = (" 
           <<via2turnMinLen[i][0] <<", "
           <<via2turnMinLen[i][1] <<", "
           <<via2turnMinLen[i][2] <<", "
           <<via2turnMinLen[i][3] <<")" <<endl;
    }
    i++;
  }

  // check minstep
  i = 0;
  for (auto lNum = bottomLayerNum; lNum <= topLayerNum; lNum++) {
    if (getDesign()->getTech()->getLayer(lNum)->getType() != frLayerTypeEnum::ROUTING) {
      continue;
    }
    frViaDef* downVia = nullptr;
    frViaDef* upVia   = nullptr;
    if (getDesign()->getTech()->getBottomLayerNum() <= lNum - 1) {
      downVia = getDesign()->getTech()->getLayer(lNum - 1)->getDefaultViaDef();
    }
    if (getDesign()->getTech()->getTopLayerNum() >= lNum + 1) {
      upVia = getDesign()->getTech()->getLayer(lNum + 1)->getDefaultViaDef();
    }
    vector<frCoord> via2turnMinLenTmp(4, 0);
    via2turnMinLen[i][0] = max(via2turnMinLen[i][0], init_via2turnMinLen_minStp(lNum, downVia, false));
    via2turnMinLen[i][1] = max(via2turnMinLen[i][1], init_via2turnMinLen_minStp(lNum, downVia, true));
    via2turnMinLen[i][2] = max(via2turnMinLen[i][2], init_via2turnMinLen_minStp(lNum, upVia, false));
    via2turnMinLen[i][3] = max(via2turnMinLen[i][3], init_via2turnMinLen_minStp(lNum, upVia, true));
    if (enableOutput) {
      cout <<"initVia2TurnMinLen_minstep " <<getDesign()->getTech()->getLayer(lNum)->getName()
           <<" (down->x->y, down->y->x, up->x->y, up->y->x) = (" 
           <<via2turnMinLen[i][0] <<", "
           <<via2turnMinLen[i][1] <<", "
           <<via2turnMinLen[i][2] <<", "
           <<via2turnMinLen[i][3] <<")" <<endl;
    }
    i++;
  }
}



void FlexDR::init() {
  frTime t;
  if (VERBOSE > 0) {
    cout <<endl <<"start routing data preparation" <<endl;
  }
  initGCell2BoundaryPin();
  getRegionQuery()->initDRObj(getTech()->getLayers().size()); // first init in postProcess

  init_halfViaEncArea();
  init_via2viaMinLen();
  init_via2viaMinLenNew();
  init_via2turnMinLen();

  if (VERBOSE > 0) {
    t.print();
  }
}

void FlexDR::removeGCell2BoundaryPin() {
  gcell2BoundaryPin.clear();
  gcell2BoundaryPin.shrink_to_fit();
}

map<frNet*, set<pair<frPoint, frLayerNum> >, frBlockObjectComp> FlexDR::initDR_mergeBoundaryPin(int startX, int startY, int size, const frBox &routeBox) {
  map<frNet*, set<pair<frPoint, frLayerNum> >, frBlockObjectComp> bp;
  auto gCellPatterns = getDesign()->getTopBlock()->getGCellPatterns();
  auto &xgp = gCellPatterns.at(0);
  auto &ygp = gCellPatterns.at(1);
  for (int i = startX; i < (int)xgp.getCount() && i < startX + size; i++) {
    for (int j = startY; j < (int)ygp.getCount() && j < startY + size; j++) {
      auto &currBp = gcell2BoundaryPin[i][j];
      for (auto &[net, s]: currBp) {
        for (auto &[pt, lNum]: s) {
          if (pt.x() == routeBox.left()   || pt.x() == routeBox.right() ||
              pt.y() == routeBox.bottom() || pt.y() == routeBox.top()) {
            bp[net].insert(make_pair(pt, lNum));
          }
        }
      }
    }
  }
  return bp;
}

void FlexDR::initDR(int size, bool enableDRC) {
  std::cout << "initDR Erfan\n";
  bool TEST = false;
  // bool TEST = true;
  //FlexGridGraph gg(getTech(), getDesign());
  ////frBox testBBox(225000, 228100, 228000, 231100); // net1702 in ispd19_test1
  //frBox testBBox(0, 0, 2000, 2000); // net1702 in ispd19_test1
  ////gg.setBBox(testBBox);
  //gg.init(testBBox);
  //gg.print();
  //exit(1);

  frTime t;

  if (VERBOSE > 0) {
    cout <<endl <<"start initial detail routing ..." <<endl;
  }
  frBox dieBox;
  getDesign()->getTopBlock()->getBoundaryBBox(dieBox);

  auto gCellPatterns = getDesign()->getTopBlock()->getGCellPatterns();
  auto &xgp = gCellPatterns.at(0);
  auto &ygp = gCellPatterns.at(1);

  int cnt = 0;
  int tot = (((int)xgp.getCount() - 1) / size + 1) * (((int)ygp.getCount() - 1) / size + 1);
  int prev_perc = 0;
  bool isExceed = false;

  int numQuickMarkers = 0;
  if (TEST) {
    //FlexDRWorker worker(getDesign());
    FlexDRWorker worker(this);
    //frBox routeBox;
    //routeBox.set(0*2000, 0*2000, 1*2000, 1*2000);
    //frCoord xl = 21 * 2000;
    //frCoord yl = 42 * 2000;
    frCoord xl = 241.92 * 2000;
    frCoord yl = 241.92 * 2000;
    //frCoord xh = 129 * 2000;
    //frCoord yh = 94.05 * 2000;
    frPoint idx;
    getDesign()->getTopBlock()->getGCellIdx(frPoint(xl, yl), idx);
    if (VERBOSE > 1) {
      cout <<"(i,j) = (" <<idx.x() <<", " <<idx.y() <<")" <<endl;
    }
    //getDesign()->getTopBlock()->getGCellBox(idx, routeBox);
    frBox routeBox1;
    getDesign()->getTopBlock()->getGCellBox(frPoint(idx.x(), idx.y()), routeBox1);
    frBox routeBox2;
    getDesign()->getTopBlock()->getGCellBox(frPoint(min((int)xgp.getCount() - 1, idx.x() + size-1), 
                                                    min((int)ygp.getCount(), idx.y() + size-1)), routeBox2);
    frBox routeBox(routeBox1.left(), routeBox1.bottom(), routeBox2.right(), routeBox2.top());
    auto bp = initDR_mergeBoundaryPin(idx.x(), idx.y(), size, routeBox);
    //routeBox.set(129*2000, 94.05*2000, 132*2000, 96.9*2000);
    worker.setRouteBox(routeBox);
    frBox extBox;
    routeBox.bloat(2000, extBox);
    frBox drcBox;
    routeBox.bloat(500, drcBox);
    worker.setRouteBox(routeBox);
    worker.setExtBox(extBox);
    worker.setDrcBox(drcBox);
    worker.setFollowGuide(false);
    worker.setCost(DRCCOST, 0, 0, 0);
    //int i = (129   * 2000 - xgp.getStartCoord()) / xgp.getSpacing();
    //int j = (94.05 * 2000 - ygp.getStartCoord()) / ygp.getSpacing();
    //worker.setDRIter(0, gcell2BoundaryPin[idx.x()][idx.y()]);
    worker.setDRIter(0, bp);
    worker.setEnableDRC(enableDRC);
    //worker.setTest(true);
    worker.main();
    cout <<"done"  <<endl <<flush;
  } else {
    vector<unique_ptr<FlexDRWorker> > uworkers;
    int batchStepX, batchStepY;

    getBatchInfo(batchStepX, batchStepY);

    vector<vector<vector<unique_ptr<FlexDRWorker> > > > workers(batchStepX * batchStepY);

    int xIdx = 0, yIdx = 0;
    // sequential init
    for (int i = 0; i < (int)xgp.getCount(); i += size) {
      for (int j = 0; j < (int)ygp.getCount(); j += size) {
        auto worker = make_unique<FlexDRWorker>(this);
        frBox routeBox1;
        getDesign()->getTopBlock()->getGCellBox(frPoint(i, j), routeBox1);
        frBox routeBox2;
        getDesign()->getTopBlock()->getGCellBox(frPoint(min((int)xgp.getCount() - 1, i + size-1), 
                                                        min((int)ygp.getCount(), j + size-1)), routeBox2);
        //frBox routeBox;
        frBox routeBox(routeBox1.left(), routeBox1.bottom(), routeBox2.right(), routeBox2.top());
        frBox extBox;
        routeBox.bloat(MTSAFEDIST, extBox);
        frBox drcBox;
        routeBox.bloat(DRCSAFEDIST, drcBox);
        worker->setRouteBox(routeBox);
        worker->setExtBox(extBox);
        worker->setDrcBox(drcBox);

        auto bp = initDR_mergeBoundaryPin(i, j, size, routeBox);
        worker->setDRIter(0, bp);
        // set boundary pin
        worker->setEnableDRC(enableDRC);
        worker->setFollowGuide(false);
        //worker->setFollowGuide(true);
        worker->setCost(DRCCOST, 0, 0, 0);
        // int workerIdx = xIdx * batchSizeY + yIdx;
        int batchIdx = (xIdx % batchStepX) * batchStepY + yIdx % batchStepY;
        // workers[batchIdx][workerIdx] = worker;
        if (workers[batchIdx].empty() || (int)workers[batchIdx].back().size() >= BATCHSIZE) {
          workers[batchIdx].push_back(vector<unique_ptr<FlexDRWorker> >());
        }
        workers[batchIdx].back().push_back(std::move(worker));

        yIdx++;
      }
      yIdx = 0;
      xIdx++;
    }

    omp_set_num_threads(MAX_THREADS);

    // parallel execution
    for (auto &workerBatch: workers) {
      for (auto &workersInBatch: workerBatch) {
        // multi thread
        #pragma omp parallel for schedule(dynamic)
        for (int i = 0; i < (int)workersInBatch.size(); i++) {
          // workersInBatch[i]->main_mt();
          workersInBatch[i]->main_mt();
          #pragma omp critical 
          {
            cnt++;
            if (VERBOSE > 0) {
              if (cnt * 1.0 / tot >= prev_perc / 100.0 + 0.1 && prev_perc < 90) {
                if (prev_perc == 0 && t.isExceed(0)) {
                  isExceed = true;
                }
                prev_perc += 10;
                //if (true) {
                if (isExceed) {
                  if (enableDRC) {
                    cout <<"    completing1 " <<prev_perc <<"% with " <<getDesign()->getTopBlock()->getNumMarkers() <<" violations" <<endl;
                  } else {
                    cout <<"    completing2 " <<prev_perc <<"% with " <<numQuickMarkers <<" quick violations" <<endl;
                  }
                  cout <<"    " <<t <<endl <<flush;
                }
              }
            }
          }
        }
        // single thread
        for (int i = 0; i < (int)workersInBatch.size(); i++) {
          workersInBatch[i]->end();
        }
        workersInBatch.clear();
      }
    }
    checkConnectivity();
  }
  if (VERBOSE > 0) {
    if (cnt * 1.0 / tot >= prev_perc / 100.0 + 0.1 && prev_perc >= 90) {
      if (prev_perc == 0 && t.isExceed(0)) {
        isExceed = true;
      }
      prev_perc += 10;
      //if (true) {
      if (isExceed) {
        if (enableDRC) {
          cout <<"    completing3 " <<prev_perc <<"% with " <<getDesign()->getTopBlock()->getNumMarkers() <<" violations" <<endl;
        } else {
          cout <<"    completing4 " <<prev_perc <<"% with " <<numQuickMarkers <<" quick violations" <<endl;
        }
        cout <<"    " <<t <<endl <<flush;
      }
    }
  }

  //cout <<"  number of violations = " <<numMarkers <<endl;
  removeGCell2BoundaryPin();
  numViols.push_back(getDesign()->getTopBlock()->getNumMarkers());
  if (VERBOSE > 0) {
    if (enableDRC) {
      cout <<"  number of violations = "       <<getDesign()->getTopBlock()->getNumMarkers() <<endl;
    } else {
      cout <<"  number of quick violations = " <<numQuickMarkers <<endl;
    }
    t.print();
    cout <<flush;
  }
}



void FlexDR::getBatchInfo(int &batchStepX, int &batchStepY) {
  batchStepX = 2;
  batchStepY = 2;
}

void FlexDR::searchRepair(int iter, int size, int offset, int mazeEndIter, 
                          frUInt4 workerDRCCost, frUInt4 workerMarkerCost, 
                          frUInt4 workerMarkerBloatWidth, frUInt4 workerMarkerBloatDepth,
                          bool enableDRC, int ripupMode, bool followGuide, 
                          int fixMode, bool TEST) {
  if (iter > END_ITERATION) {
    return;
  }
  if (ripupMode != 1 && getDesign()->getTopBlock()->getMarkers().size() == 0) {
    return;
  } 

  frTime t;
  //bool TEST = false;
  //bool TEST = true;
  if (VERBOSE > 0) {
    cout <<endl <<"start " <<iter;
    string suffix;
    if (iter == 1 || (iter > 20 && iter % 10 == 1)) {
      suffix = "st";
    } else if (iter == 2 || (iter > 20 && iter % 10 == 2)) {
      suffix = "nd";
    } else if (iter == 3 || (iter > 20 && iter % 10 == 3)) {
      suffix = "rd";
    } else {
      suffix = "th";
    }
    cout <<suffix <<" optimization iteration ..." <<endl;
  }
  frBox dieBox;
  getDesign()->getTopBlock()->getBoundaryBBox(dieBox);
  auto gCellPatterns = getDesign()->getTopBlock()->getGCellPatterns();
  auto &xgp = gCellPatterns.at(0);
  auto &ygp = gCellPatterns.at(1);
  int numQuickMarkers = 0;
  int clipSize = size;
  int cnt = 0;
  int tot = (((int)xgp.getCount() - 1 - offset) / clipSize + 1) * (((int)ygp.getCount() - 1 - offset) / clipSize + 1);
  int prev_perc = 0;
  bool isExceed = false;
  if (TEST) {
    cout <<"search and repair test mode" <<endl <<flush;
    //FlexDRWorker worker(getDesign());
    FlexDRWorker worker(this);
    frBox routeBox;
    //frCoord xl = 148.5 * 2000;
    //frCoord yl = 570 * 2000;
    //frPoint idx;
    //getDesign()->getTopBlock()->getGCellIdx(frPoint(xl, yl), idx);
    //if (VERBOSE > 1) {
    //  cout <<"(i,j) = (" <<idx.x() <<", " <<idx.y() <<")" <<endl;
    //}
    //getDesign()->getTopBlock()->getGCellBox(idx, routeBox);
    //routeBox.set(156*2000, 108.3*2000, 177*2000, 128.25*2000);
    // routeBox.set(175*2000, 3.5*2000, 185*2000, 13.5*2000);
    // routeBox.set(0*2000, 0*2000, 200*2000, 200*2000);
    // routeBox.set(420*1000, 816.1*1000, 441*1000, 837.1*1000);
    routeBox.set(441*1000, 816.1*1000, 462*1000, 837.1*1000);
    worker.setRouteBox(routeBox);
    frBox extBox;
    frBox drcBox;
    routeBox.bloat(2000, extBox);
    routeBox.bloat(500, drcBox);
    worker.setRouteBox(routeBox);
    worker.setExtBox(extBox);
    worker.setDrcBox(drcBox);
    worker.setMazeEndIter(mazeEndIter);
    worker.setTest(true);
    //worker.setQuickDRCTest(true);
    //worker.setDRCTest(true);
    worker.setDRIter(iter);
    if (!iter) {
      // set boundary pin (need to manulally calculate for test mode)
      auto bp = initDR_mergeBoundaryPin(147, 273, size, routeBox);
      worker.setDRIter(0, bp);
    }
    worker.setEnableDRC(enableDRC);
    worker.setRipupMode(ripupMode);
    worker.setFollowGuide(followGuide);
    worker.setFixMode(fixMode);
    //worker.setNetOrderingMode(netOrderingMode);
    worker.setCost(workerDRCCost, workerMarkerCost, workerMarkerBloatWidth, workerMarkerBloatDepth);
    worker.main_mt();
    numQuickMarkers += worker.getNumQuickMarkers();
    cout <<"done"  <<endl <<flush;
  } else {

    vector<unique_ptr<FlexDRWorker> > uworkers;
    int batchStepX, batchStepY;

    getBatchInfo(batchStepX, batchStepY);

    vector<vector<vector<unique_ptr<FlexDRWorker> > > > workers(batchStepX * batchStepY);

    int xIdx = 0, yIdx = 0;
    for (int i = offset; i < (int)xgp.getCount(); i += clipSize) {
      for (int j = offset; j < (int)ygp.getCount(); j += clipSize) {
        auto worker = make_unique<FlexDRWorker>(this);
        frBox routeBox1;
        getDesign()->getTopBlock()->getGCellBox(frPoint(i, j), routeBox1);
        frBox routeBox2;
        getDesign()->getTopBlock()->getGCellBox(frPoint(min((int)xgp.getCount() - 1, i + clipSize-1), 
                                                        min((int)ygp.getCount(), j + clipSize-1)), routeBox2);
        frBox routeBox(routeBox1.left(), routeBox1.bottom(), routeBox2.right(), routeBox2.top());
        frBox extBox;
        frBox drcBox;
        routeBox.bloat(MTSAFEDIST, extBox);
        routeBox.bloat(DRCSAFEDIST, drcBox);
        worker->setRouteBox(routeBox);
        worker->setExtBox(extBox);
        worker->setDrcBox(drcBox);
        worker->setMazeEndIter(mazeEndIter);
        worker->setDRIter(iter);
        if (!iter) {
          // if (routeBox.left() == 441000 && routeBox.bottom() == 816100) {
          //   cout << "@@@ debug: " << i << " " << j << endl;
          // }
          // set boundary pin
          auto bp = initDR_mergeBoundaryPin(i, j, size, routeBox);
          worker->setDRIter(0, bp);
        }
        worker->setEnableDRC(enableDRC);
        worker->setRipupMode(ripupMode);
        worker->setFollowGuide(followGuide);
        worker->setFixMode(fixMode);
        worker->setCost(workerDRCCost, workerMarkerCost, workerMarkerBloatWidth, workerMarkerBloatDepth);

        int batchIdx = (xIdx % batchStepX) * batchStepY + yIdx % batchStepY;
        if (workers[batchIdx].empty() || (int)workers[batchIdx].back().size() >= BATCHSIZE) {
          workers[batchIdx].push_back(vector<unique_ptr<FlexDRWorker> >());
        }
        workers[batchIdx].back().push_back(std::move(worker));

        yIdx++;
      }
      yIdx = 0;
      xIdx++;
    }


    omp_set_num_threads(MAX_THREADS);

    // parallel execution
    for (auto &workerBatch: workers) {
      for (auto &workersInBatch: workerBatch) {
        // multi thread
        #pragma omp parallel for schedule(dynamic)
        for (int i = 0; i < (int)workersInBatch.size(); i++) {
          workersInBatch[i]->main_mt();
          #pragma omp critical 
          {
            cnt++;
            if (VERBOSE > 0) {
              if (cnt * 1.0 / tot >= prev_perc / 100.0 + 0.1 && prev_perc < 90) {
                if (prev_perc == 0 && t.isExceed(0)) {
                  isExceed = true;
                }
                prev_perc += 10;
                //if (true) {
                if (isExceed) {
                  if (enableDRC) {
                    cout <<"    completing5 " <<prev_perc <<"% with " <<getDesign()->getTopBlock()->getNumMarkers() <<" violations" <<endl;
                  } else {
                    cout <<"    completing6 " <<prev_perc <<"% with " <<numQuickMarkers <<" quick violations" <<endl;
                  }
                  cout <<"    " <<t <<endl <<flush;
                }
              }
            }
          }
        }
        // single thread
        for (int i = 0; i < (int)workersInBatch.size(); i++) {
          workersInBatch[i]->end();
        }
        workersInBatch.clear();
      }
    }
  }
  if (!iter) {
    removeGCell2BoundaryPin();
  }
  if (VERBOSE > 0) {
    if (cnt * 1.0 / tot >= prev_perc / 100.0 + 0.1 && prev_perc >= 90) {
      if (prev_perc == 0 && t.isExceed(0)) {
        isExceed = true;
      }
      prev_perc += 10;
      //if (true) {
      if (isExceed) {
        if (enableDRC) {
          cout <<"    completing7 " <<prev_perc <<"% with " <<getDesign()->getTopBlock()->getNumMarkers() <<" violations" <<endl;
        } else {
          cout <<"    completing8 " <<prev_perc <<"% with " <<numQuickMarkers <<" quick violations" <<endl;
        }
        cout <<"    " <<t <<endl <<flush;
      }
    }
  }
  checkConnectivity(iter);
  numViols.push_back(getDesign()->getTopBlock()->getNumMarkers());
  if (VERBOSE > 0) {
    if (enableDRC) {
      cout <<"  number of violations = " <<getDesign()->getTopBlock()->getNumMarkers() <<endl;
    } else {
      cout <<"  number of quick violations = " <<numQuickMarkers <<endl;
    }
    t.print();
    cout <<flush;
  }
  end();
}


void FlexDR::searchFeatureExtraction(int iter, int size, int offset, int mazeEndIter, 
                          frUInt4 workerDRCCost, frUInt4 workerMarkerCost, 
                          frUInt4 workerMarkerBloatWidth, frUInt4 workerMarkerBloatDepth,
                          bool enableDRC, int ripupMode, bool followGuide, 
                          int fixMode, bool TEST) {
  std::cout << "Feature Extraction ....\n";
                    
  if (iter > END_ITERATION) {
    return;
  }
  if (ripupMode != 1 && getDesign()->getTopBlock()->getMarkers().size() == 0) {
    return;
  } 

  frTime t;
  //bool TEST = false;
  //bool TEST = true;
  if (VERBOSE > 0) {
    cout <<endl <<"start " <<iter;
    string suffix;
    if (iter == 1 || (iter > 20 && iter % 10 == 1)) {
      suffix = "st";
    } else if (iter == 2 || (iter > 20 && iter % 10 == 2)) {
      suffix = "nd";
    } else if (iter == 3 || (iter > 20 && iter % 10 == 3)) {
      suffix = "rd";
    } else {
      suffix = "th";
    }
    cout <<suffix <<" optimization iteration ..." <<endl;
  }
  frBox dieBox;
  getDesign()->getTopBlock()->getBoundaryBBox(dieBox);
  auto gCellPatterns = getDesign()->getTopBlock()->getGCellPatterns();
  auto &xgp = gCellPatterns.at(0);
  auto &ygp = gCellPatterns.at(1);
  int numQuickMarkers = 0;
  int clipSize = size;
  int cnt = 0;
  int tot = (((int)xgp.getCount() - 1 - offset) / clipSize + 1) * (((int)ygp.getCount() - 1 - offset) / clipSize + 1);
  int prev_perc = 0;
  bool isExceed = false;
  if (TEST) {
    cout <<"search and repair test mode" <<endl <<flush;
    //FlexDRWorker worker(getDesign());
    FlexDRWorker worker(this);
    frBox routeBox;
    //frCoord xl = 148.5 * 2000;
    //frCoord yl = 570 * 2000;
    //frPoint idx;
    //getDesign()->getTopBlock()->getGCellIdx(frPoint(xl, yl), idx);
    //if (VERBOSE > 1) {
    //  cout <<"(i,j) = (" <<idx.x() <<", " <<idx.y() <<")" <<endl;
    //}
    //getDesign()->getTopBlock()->getGCellBox(idx, routeBox);
    //routeBox.set(156*2000, 108.3*2000, 177*2000, 128.25*2000);
    // routeBox.set(175*2000, 3.5*2000, 185*2000, 13.5*2000);
    // routeBox.set(0*2000, 0*2000, 200*2000, 200*2000);
    // routeBox.set(420*1000, 816.1*1000, 441*1000, 837.1*1000);
    routeBox.set(441*1000, 816.1*1000, 462*1000, 837.1*1000);
    worker.setRouteBox(routeBox);
    frBox extBox;
    frBox drcBox;
    routeBox.bloat(2000, extBox);
    routeBox.bloat(500, drcBox);
    worker.setRouteBox(routeBox);
    worker.setExtBox(extBox);
    worker.setDrcBox(drcBox);
    worker.setMazeEndIter(mazeEndIter);
    worker.setTest(true);
    //worker.setQuickDRCTest(true);
    //worker.setDRCTest(true);
    worker.setDRIter(iter);
    if (!iter) {
      // set boundary pin (need to manulally calculate for test mode)
      auto bp = initDR_mergeBoundaryPin(147, 273, size, routeBox);
      worker.setDRIter(0, bp);
    }
    worker.setEnableDRC(enableDRC);
    worker.setRipupMode(ripupMode);
    worker.setFollowGuide(followGuide);
    worker.setFixMode(fixMode);
    //worker.setNetOrderingMode(netOrderingMode);
    worker.setCost(workerDRCCost, workerMarkerCost, workerMarkerBloatWidth, workerMarkerBloatDepth);
    worker.main_mt();
    numQuickMarkers += worker.getNumQuickMarkers();
    cout <<"done"  <<endl <<flush;
  } else {

    vector<unique_ptr<FlexDRWorker> > uworkers;
    int batchStepX, batchStepY;

    getBatchInfo(batchStepX, batchStepY);

    vector<vector<vector<unique_ptr<FlexDRWorker> > > > workers(batchStepX * batchStepY);

    int xIdx = 0, yIdx = 0;
    for (int i = offset; i < (int)xgp.getCount(); i += clipSize) {
      for (int j = offset; j < (int)ygp.getCount(); j += clipSize) {
        auto worker = make_unique<FlexDRWorker>(this);
        frBox routeBox1;
        getDesign()->getTopBlock()->getGCellBox(frPoint(i, j), routeBox1);
        frBox routeBox2;
        getDesign()->getTopBlock()->getGCellBox(frPoint(min((int)xgp.getCount() - 1, i + clipSize-1), 
                                                        min((int)ygp.getCount(), j + clipSize-1)), routeBox2);
        frBox routeBox(routeBox1.left(), routeBox1.bottom(), routeBox2.right(), routeBox2.top());
        frBox extBox;
        frBox drcBox;
        routeBox.bloat(MTSAFEDIST, extBox);
        routeBox.bloat(DRCSAFEDIST, drcBox);

        worker->setIJ(i,j);


        // std::cout << "i: " << i << ", j: " << j << std::endl;

        // std::cout << "routeBox: ( " << routeBox.lowerLeft().x() << ", "
        //                              << routeBox.lowerLeft().y() << ", "
        //                              << routeBox.upperRight().x() << ", "
        //                              << routeBox.upperRight().y() << ")\n";
        
        // std::cout << "extBox: ( "    << extBox.lowerLeft().x() << ", "
        //                              << extBox.lowerLeft().y() << ", "
        //                              << extBox.upperRight().x() << ", "
        //                              << extBox.upperRight().y() << ")\n";

        // std::cout << "drcBox: ( "    << drcBox.lowerLeft().x() << ", "
        //                              << drcBox.lowerLeft().y() << ", "
        //                              << drcBox.upperRight().x() << ", "
        //                              << drcBox.upperRight().y() << ")\n";

        
        worker->setRouteBox(routeBox);
        worker->setExtBox(extBox);
        worker->setDrcBox(drcBox);
        worker->setMazeEndIter(mazeEndIter);
        worker->setDRIter(iter);
        if (!iter) {
          // if (routeBox.left() == 441000 && routeBox.bottom() == 816100) {
          //   cout << "@@@ debug: " << i << " " << j << endl;
          // }
          // set boundary pin
          auto bp = initDR_mergeBoundaryPin(i, j, size, routeBox);
          worker->setDRIter(0, bp);
        }
        worker->setEnableDRC(enableDRC);
        worker->setRipupMode(ripupMode);
        worker->setFollowGuide(followGuide);
        worker->setFixMode(fixMode);
        worker->setCost(workerDRCCost, workerMarkerCost, workerMarkerBloatWidth, workerMarkerBloatDepth);

        int batchIdx = (xIdx % batchStepX) * batchStepY + yIdx % batchStepY;
        if (workers[batchIdx].empty() || (int)workers[batchIdx].back().size() >= BATCHSIZE) {
          workers[batchIdx].push_back(vector<unique_ptr<FlexDRWorker> >());
        }
        workers[batchIdx].back().push_back(std::move(worker));

        yIdx++;
      }
      yIdx = 0;
      xIdx++;
    }// end for 

    omp_set_num_threads(MAX_THREADS);

    // parallel execution
    for (auto &workerBatch: workers) {
      for (auto &workersInBatch: workerBatch) {
        // multi thread
        #pragma omp parallel for schedule(dynamic)
        for (int i = 0; i < (int)workersInBatch.size(); i++) {
          // workersInBatch[i]->main_mt();
          workersInBatch[i]->main_mt_feature_extraction();
          #pragma omp critical 
          {
            cnt++;
            if (VERBOSE > 0) {
              if (cnt * 1.0 / tot >= prev_perc / 100.0 + 0.1 && prev_perc < 90) {
                if (prev_perc == 0 && t.isExceed(0)) {
                  isExceed = true;
                }
                prev_perc += 10;
                //if (true) {
                if (isExceed) {
                  if (enableDRC) {
                    cout <<"    completing5 " <<prev_perc <<"% with " <<getDesign()->getTopBlock()->getNumMarkers() <<" violations" <<endl;
                  } else {
                    cout <<"    completing6 " <<prev_perc <<"% with " <<numQuickMarkers <<" quick violations" <<endl;
                  }
                  cout <<"    " <<t <<endl <<flush;
                }
              }
            }
          }
        }
        // single thread
        // for (int i = 0; i < (int)workersInBatch.size(); i++) {
        //   workersInBatch[i]->end();
        // }
        // workersInBatch.clear();
      }
    }
    
    std::cout << "print workers feature Extraction\n";

    // calculate total cell density 
    cellDensity();
    netDensity();
    blkDensity();
    writeFeatureExtraction(workers);
    CellFanOutIn();
    // applyMovement();

  }//end if else
  if (!iter) {
    removeGCell2BoundaryPin();
  }
  if (VERBOSE > 0) {
    if (cnt * 1.0 / tot >= prev_perc / 100.0 + 0.1 && prev_perc >= 90) {
      if (prev_perc == 0 && t.isExceed(0)) {
        isExceed = true;
      }
      prev_perc += 10;
      //if (true) {
      if (isExceed) {
        if (enableDRC) {
          cout <<"    completing7 " <<prev_perc <<"% with " <<getDesign()->getTopBlock()->getNumMarkers() <<" violations" <<endl;
        } else {
          cout <<"    completing8 " <<prev_perc <<"% with " <<numQuickMarkers <<" quick violations" <<endl;
        }
        cout <<"    " <<t <<endl <<flush;
      }
    }
  }
  checkConnectivity(iter);
  numViols.push_back(getDesign()->getTopBlock()->getNumMarkers());
  if (VERBOSE > 0) {
    if (enableDRC) {
      cout <<"  number of violations = " <<getDesign()->getTopBlock()->getNumMarkers() <<endl;
    } else {
      cout <<"  number of quick violations = " <<numQuickMarkers <<endl;
    }
    t.print();
    cout <<flush;
  }
  end();
}

void FlexDR::CellFanOutIn(){
  bool enableOutput = false;
  std::vector<std::vector<std::string>> rotation_table_normal = { 
    {"frcR0"  , "frcR270", "frcR180", "frcR90" },
    {"frcR90" , "frcR0"  , "frcR270", "frcR180"}, 
    {"frcR180", "frcR90" , "frcR0"  , "frcR270"},
    {"frcR270", "frcR180", "frcR90" , "frcR0"  }
  };

  std::vector<std::vector<std::string>> rotation_table_flipped = { 
    {"frcMXR90"  , "frcMY"     , "frcMYR90" , "frcMX" },
    {"frcMY"     , "frcMYR90"  , "frcMX"    , "frcMXR90"}, 
    {"frcMYR90"  , "frcMX"     , "frcMXR90" , "frcMY"},
    {"frcMX"     , "frcMXR90"  , "frcMY"    , "frcMYR90"}
  };

  // cell name to number of access points in each parition
  std::map<std::string,std::vector<int>> partitions_pin_aps_map;

  struct PinPartMD{
    std::string pin_name;
    int partition_idx;
    long int md;
  };//end struct PinIdMD

  std::set<std::string> macros_set = {
    "AOI22X1"
  };
  enum PartEnum {PART1,PART2,PART3,PART4};
  std::vector<frPoint> partitions_coord;
  partitions_coord.resize(4);
  std::vector<long int> partitions_manhattan_distance;
  partitions_manhattan_distance.resize(4);

  auto manhattanDistance = [](frPoint p1,frPoint p2){
    return std::abs(p1.x()-p2.x()) + std::abs(p1.y()-p2.y());
  };//end manhattanDistance lamda function 

  auto getCenterInst = [](frInst* const inst,frPoint& point_center){
      bool enableOutput = false;
      frPoint inst_origin;
      frOrient r0 = frcR0;
      frOrient r_current = inst->getOrient();
      inst->setOrient(r0);
      const auto ref_blk = inst->getRefBlock();
      frBox ref_box;
      ref_blk->getBBox(ref_box);
      inst->getOrigin(inst_origin);
      if(enableOutput){
        std::cout << "orient: " << inst->getOrient() << std::endl;
      }


      int length = ref_box.length();
      int width = ref_box.width();
      if(enableOutput){
        std::cout << "inst: " << inst->getName() << " orig: " << inst_origin.x() << ", " << inst_origin.y() 
                  << " width: " << width << ", len: " << length << std::endl;
      }
      if(width == 3420)
        point_center.set(inst_origin.x() + (length/2.0),inst_origin.y()+1710);
      else
        point_center.set(inst_origin.x() + (width/2.0),inst_origin.y()+1710);

      inst->setOrient(r_current);
  };//end getCenterInst

  auto initPartitions = [&getCenterInst](std::vector<frPoint>& partitions_coord,std::unique_ptr<frInst> const &inst){
    bool enableOutput = false;
    int constant_length_of_instance = 3420;

    frBox inst_box;
    inst->getBBox(inst_box);
    int length = inst_box.length();
    int width = inst_box.width();
    if(length != constant_length_of_instance){
      auto tmp = length;
      length = width;
      width = tmp;
    }

    frPoint center;
    getCenterInst(inst.get(),center);
    partitions_coord[PART1].set(center.x()-(width/4.0) , center.y()-(length/4.0));
    partitions_coord[PART2].set(center.x()+(width/4.0) , center.y()-(length/4.0));
    partitions_coord[PART3].set(center.x()+(width/4.0) , center.y()+(length/4.0));
    partitions_coord[PART4].set(center.x()-(width/4.0) , center.y()+(length/4.0));
    enableOutput = true;
    if(enableOutput){
      std::cout << "inst name: " << inst->getName() << std::endl;
      std::cout << "center: " << center.x() << ", " << center.y() << std::endl;
      std::cout << "width: " << width << ", len: " << length << std::endl;
      int i =0;
      for(auto part : partitions_coord){
        i++;
        std::cout << "part" << i <<": " << part.x() << ", " << part.y() << std::endl;
      }
    }
    enableOutput = false;
  };//end initpartitions lambda function

  auto initPartitionsBlk = [](std::vector<frBox>& partitions_box,std::unique_ptr<frBlock> const &blk){
    bool enableOutput = false;
    partitions_box.resize(4);
    frBox blk_box;
    blk->getBBox(blk_box);
    frPoint inst_origin(0,0);
    int length = blk_box.length();
    int width = blk_box.width();
    frBox part1_box(blk_box.lowerLeft().x(),blk_box.lowerLeft().y(),
                    blk_box.upperRight().x()-(width/2.0),blk_box.upperRight().y()-(length/2.0));

    frBox part2_box(blk_box.lowerLeft().x()+(width/2.0),blk_box.lowerLeft().y(),
                    blk_box.upperRight().x(),blk_box.upperRight().y()-(length/2.0));

    frBox part3_box(blk_box.lowerLeft().x()+(width/2.0),blk_box.lowerLeft().y()+(length/2.0),
                    blk_box.upperRight().x(),blk_box.upperRight().y());

    frBox part4_box(blk_box.lowerLeft().x(),blk_box.lowerLeft().y()+(length/2.0),
                    blk_box.upperRight().x()-(width/2.0),blk_box.upperRight().y());  
    partitions_box[PART1].set(part1_box);
    partitions_box[PART2].set(part2_box);
    partitions_box[PART3].set(part3_box);
    partitions_box[PART4].set(part4_box);
    if(enableOutput){
      double DBU_tmp = 2000.0;
      int i = 0;
      for(auto box : partitions_box){
        i++;
        std::cout << "part_box_blk" <<i << ": " << box.lowerLeft().x()/DBU_tmp << ", "
                                                << box.lowerLeft().y()/DBU_tmp << ", "
                                                << box.upperRight().x()/DBU_tmp << ", "
                                                << box.upperRight().y()/DBU_tmp << "\n";

      }//end for 
    }
  };//end initpartitions lambda function

  auto ManhattanDistanceNets = [&getCenterInst,&manhattanDistance](std::vector<frPoint>& partitions,
                                  std::map<std::string,std::vector<long int>>& manhattan_distances_map,
                                  std::unique_ptr<frInst> const &inst){
      bool enableOutput = false;
      const auto& nets = inst->getNets();

      if(inst->getName() == "inst8642")
        enableOutput = true;
             
      for(auto& net : nets){
        
        std::vector<long int> manhattan_distances;
        manhattan_distances.resize(4);
        std::string pin_current_inst = "";
        if(enableOutput){
          std::cout << "net: " << net->getName() << std::endl;
          std::cout << "number of connection: " << net->getInstTerms().size() << std::endl;
        } //end if 
        const auto& inst_terms = net->getInstTerms();
        for(auto& inst_term : inst_terms){
          frPoint inst_net_center;
          const auto& inst_net = inst_term->getInst();
          if(enableOutput){
            std::cout << "inst name: " << inst_net->getName() << std::endl;
            std::cout << "pin name: " << inst_term->getTerm()->getName() << std::endl;
          }
          if(inst_net->getName() == inst->getName()){
            pin_current_inst = inst_term->getTerm()->getName();
            if(enableOutput){
              std::cout << "pin_current_inst set to: " << inst_term->getTerm()->getName() << std::endl;
            }
          }
          getCenterInst(inst_net,inst_net_center);
          int i = 0;
          if(enableOutput)  {
            std::cout << "before md: \n" ;
            for(auto md : manhattan_distances){
              std::cout << md << " ";
            }
            std::cout << std::endl;
          }
          for(auto part : partitions){
            manhattan_distances[i] = manhattan_distances[i] + manhattanDistance(part,inst_net_center);
            i++;
            if(enableOutput){
              std::cout << "part"  << i<< ": " << part.x() << ", " << part.y() << " to " 
                        << inst_net_center.x() << ", " << inst_net_center.y() << " md " 
                        << manhattan_distances[i-1] <<std::endl;
                        
            }
          }        
          if(enableOutput)  {
            std::cout << "current md: \n" ;
            for(auto md : manhattan_distances){
              std::cout << md << " ";
            }
            std::cout << std::endl;
          }
        }//emd inst_terms
        if(enableOutput){
          std::cout << "pin_current_inst: " << pin_current_inst << std::endl;
        }
        manhattan_distances_map[pin_current_inst] = manhattan_distances;
      }//end for nets 
      if(enableOutput){
        for(auto pin_to_md : manhattan_distances_map){
          std::cout << "pin name: " << pin_to_md.first << std::endl;
          int i = 0;
          for(auto md : pin_to_md.second){
            i++;
            std::cout << "part" << i << ": " << md << std::endl;
          }
        }
      }
      return true;
  };//end ManhattanDistanceNets lamda function

  auto getOverlapArea = [](std::vector<frBox>& part_boxes, std::map<std::string,std::vector<frBox>>& pin_box_map,
                          std::map<std::string,std::vector<long int>>& pin_parts_area_map){
    bool enableOutput = false;    
    typedef boost::geometry::model::d2::point_xy<double> point_type;
    typedef boost::geometry::model::polygon<point_type> polygon_type;
    std::queue<polygon_type> union_of_polygons;

    if(enableOutput){
      std::cout << "test...\n";
      polygon_type poly1;
      polygon_type poly2;
      boost::geometry::append ( poly1, point_type(0, 0) );
      boost::geometry::append ( poly1, point_type(0, 5) );
      boost::geometry::append ( poly1, point_type(5, 5) );
      boost::geometry::append ( poly1, point_type(5, 0) );
      std::cout << boost::geometry::dsv(poly1) << std::endl;
      std::deque<polygon_type> output;
      boost::geometry::intersection(poly1, poly2, output);
      int i = 0;
      std::cout << "green && blue:" << std::endl;
      BOOST_FOREACH(polygon_type const& p, output)
      {
          std::cout << i++ << ": " << boost::geometry::area(p) << std::endl;
      }
    }

    if(enableOutput){
      std::cout << "overlapArea...\n";
      int i = 0;
      for(auto part : part_boxes){
        i++;
        std::cout << "part"<< i <<": "  << part.lowerLeft().x() << ", "
                                        << part.lowerLeft().y() << ", "
                                        << part.upperRight().x() << ", "
                                        << part.upperRight().y() << "\n";
      }//e
      for(auto pin_map : pin_box_map){
          std::cout << "pin: " << pin_map.first << std::endl;
          for(auto box : pin_map.second){
            std::cout << "box: " << box.lowerLeft().x() << ", "
                                 << box.lowerLeft().y() << ", "
                                 << box.upperRight().x() << ", "
                                 << box.upperRight().y() << "\n";
          }//end for 
      }//end for 
    }//end if 

    std::map<std::string,polygon_type> pin_poly_map;
    
    for(auto pin_map : pin_box_map){
      if(enableOutput){
        std::cout << "pin: " << pin_map.first << std::endl;
      }
      std::vector<polygon_type> pin_polys;
      for(auto box : pin_map.second){
        auto xl = box.lowerLeft().x();
        auto yl = box.lowerLeft().y();
        auto xh = box.upperRight().x();
        auto yh = box.upperRight().y();
        polygon_type pin_poly;
        boost::geometry::append ( pin_poly, point_type(xl,yl) );
        boost::geometry::append ( pin_poly, point_type(xl,yh) );
        boost::geometry::append ( pin_poly, point_type(xh,yh) );
        boost::geometry::append ( pin_poly, point_type(xh,yl) );
        boost::geometry::append ( pin_poly, point_type(xl,yl) );
        if(enableOutput){
          std::cout << "box: "  << box.lowerLeft().x() << ", "
                              << box.lowerLeft().y() << ", "
                              << box.upperRight().x() << ", "
                              << box.upperRight().y() << "\n";
        }
        union_of_polygons.push(pin_poly);
      }//end for 


      while(!union_of_polygons.empty()){
        if(union_of_polygons.size() <= 1) break;
        auto p_curr = union_of_polygons.front();
        union_of_polygons.pop();
        auto p_next = union_of_polygons.front();
        union_of_polygons.pop();
        if(enableOutput){
          std::cout << "p_curr: " <<boost::geometry::dsv(p_curr) << std::endl;  
          std::cout << "p_next: " <<boost::geometry::dsv(p_next) << std::endl;  
        }
        
        
        std::vector<polygon_type> output;
        boost::geometry::union_(p_curr, p_next, output);
        for(auto out : output){
          if(enableOutput){
            std::cout << "p_out: " <<boost::geometry::dsv(out) << std::endl;  
          }
          union_of_polygons.push(out);
        }//end for 
      }//end while union_of_polygons.empty()
      auto poly = union_of_polygons.front();
      union_of_polygons.pop();
      if(enableOutput){
        std::cout << "union_of_polygons size: " << union_of_polygons.size() << std::endl;  
        std::cout << "final poly result: " <<boost::geometry::dsv(poly) << std::endl;  
      }//end if enableOutput
      pin_poly_map[pin_map.first] = poly;

      // part_boxes
      int i = 0;
      std::vector<long int> parts_area;
      for(auto part_box : part_boxes){
        i++;
        auto xl = part_box.lowerLeft().x();
        auto yl = part_box.lowerLeft().y();
        auto xh = part_box.upperRight().x();
        auto yh = part_box.upperRight().y();
        polygon_type part_poly;
        boost::geometry::append ( part_poly, point_type(xl,yl) );
        boost::geometry::append ( part_poly, point_type(xl,yh) );
        boost::geometry::append ( part_poly, point_type(xh,yh) );
        boost::geometry::append ( part_poly, point_type(xh,yl) );
        boost::geometry::append ( part_poly, point_type(xl,yl) );
        std::deque<polygon_type> output;
        boost::geometry::intersection(part_poly, poly, output);
        long int total_area = 0;
        BOOST_FOREACH(polygon_type const& p, output)
        {
          if(enableOutput)
            std::cout << "Part" << i << " area: " << boost::geometry::area(p) << std::endl;
          total_area = total_area + boost::geometry::area(p);
        }//end boost_foreach
        parts_area.push_back(total_area);
      }//end for part_boxes
      pin_parts_area_map[pin_map.first] = parts_area;



  }//end for pin_box_map


  if(enableOutput){
    std::cout << "pin_parts_area_map...\n";
    for(auto pin_map : pin_parts_area_map){
      std::cout << "pin: " << pin_map.first << std::endl;
      for (auto area : pin_map.second){
        std::cout << area << " ";
      }
      std::cout << std::endl;
    }
  }

  };//end getOverlapArea lamda function

  auto initTable = [&getOverlapArea](std::unique_ptr<frBlock> const &blk,std::vector<frBox>& partitions_box,
                     std::map<std::string,std::vector<int>>& partitions_aps_map,
                     std::map<std::string,std::vector<long int>>& pin_parts_area_map){
    bool enableOutput = false;
    typedef boost::geometry::model::d2::point_xy<int> point;
    typedef boost::geometry::model::polygon<point> polygon;


    auto& terms = blk->getTerms();
    frBox blk_box;
    blk->getBBox(blk_box);
    int width = blk_box.width();
    int length = blk_box.length();
    double DBU_tmp = 2000.0;
    if(enableOutput) {
      std::cout << "blk: " << blk->getName() << std::endl;
      std::cout << "width: " << width/DBU_tmp << ", len: " << length/DBU_tmp << std::endl;
    }

    std::map<std::string,std::vector<frBox>> pin_box_map;
    
    for(auto& term : terms){
      std::vector<int> aps_count;
      std::vector<frBox> boxs;
      aps_count.resize(4);

      if(enableOutput) std::cout << "term: " << term->getName() <<std::endl;
      for(auto& pin : term->getPins()){
        for(auto& fig : pin->getFigs()){
          frBox fig_box;
          fig->getBBox(fig_box);
          boxs.push_back(fig_box);

          if(enableOutput){
                std::cout << "fig_box: "  << fig_box.lowerLeft().x()/DBU_tmp<< ", "
                                          << fig_box.lowerLeft().y()/DBU_tmp<< ", "
                                          << fig_box.upperRight().x()/DBU_tmp << ", "
                                          << fig_box.upperRight().y()/DBU_tmp << "\n";
          }//end if 
          int part_idx = 0;
          for(auto part_box : partitions_box){
              if(enableOutput){
                std::cout << "part_box: " << part_box.lowerLeft().x()/DBU_tmp<< ", "
                                          << part_box.lowerLeft().y()/DBU_tmp<< ", "
                                          << part_box.upperRight().x()/DBU_tmp << ", "
                                          << part_box.upperRight().y()/DBU_tmp << "\n";
              }
              if(part_box.overlaps(fig_box)){
                aps_count[part_idx]++;
              }
              if(enableOutput){
                int j= 0;
                for(auto ap: aps_count){
                  j++;
                  std::cout << "ap_part" << j<<": " << ap << std::endl;
                }
              }
              part_idx++;
            }//end for partitios_box
          
          
        }//end for figs
                
      }//end for 
      std::string pin_name = term->getName();
      partitions_aps_map[pin_name] = aps_count;
      pin_box_map[pin_name] = boxs;
    }//end for temp
    // map pin name to the intersected areas with parition boxes 
    
    getOverlapArea(partitions_box,pin_box_map,pin_parts_area_map);




    if(enableOutput){
      std::cout << "partitions_aps_map...\n";
      for(auto part_aps : partitions_aps_map){
        std::cout << "pin: " << part_aps.first << std::endl;
        int i = 0;
        for(auto ap : part_aps.second){
          i++;
          std::cout << "part" << i << ": " << ap << std::endl;
          
        }
      }
    }


  };//end initTable


  std::map<std::string,std::vector<int>> partitions_aps_map;
  std::map<std::string,std::vector<long int>> pin_parts_area_map;
  // Start code 
  auto& ref_blks = getDesign()->getRefBlocks();
  for(auto& ref_blk : ref_blks){
    if(macros_set.find(ref_blk->getName()) != macros_set.end()){
      std::vector<frBox> partitions_box;
      initPartitionsBlk(partitions_box,ref_blk);
      initTable(ref_blk,partitions_box,partitions_aps_map,pin_parts_area_map);
    }//end if 
  }//end for 

  
  

  auto& instances = getDesign()->getTopBlock()->getInsts();
  if(enableOutput) std::cout << "CellFanoutIn...\n";
  for(auto& inst : instances ){
    std::vector<PinPartMD> pin_partition_mds;
    std::map<std::string,std::vector<long int>> manhattan_distances_map;

    auto ref_block = inst->getRefBlock();
    if(macros_set.find(ref_block->getName()) != macros_set.end()){
      enableOutput = true;
      if(enableOutput){
        std::cout << "inst name: " << inst->getName() << std::endl;
        std::cout << "block name: " << inst->getRefBlock()->getName() << std::endl;
      }      
      enableOutput = false;

      initPartitions(partitions_coord,inst);
      bool valid_dist = ManhattanDistanceNets(partitions_coord,manhattan_distances_map,inst);
      if(valid_dist){
        enableOutput = true;
        if(enableOutput) {
          std::cout << "manhattan distance map: \n";
          for(auto md_map : manhattan_distances_map){
            std::cout << "pin: " << md_map.first << std::endl;
            for(auto part_md : md_map.second){
              std::cout << part_md << " ";
            }
            std::cout << std::endl;
            auto min_elem_itr = std::min_element(md_map.second.begin(),md_map.second.end());
            std::cout << "min md in parition: " << std::distance(md_map.second.begin(),min_elem_itr) <<std::endl;
          }
          std::cout << std::endl;
        }
        enableOutput = false;
        for(auto md_map : manhattan_distances_map){
            std::cout << std::endl;
            auto min_elem_itr = std::min_element(md_map.second.begin(),md_map.second.end());
            PinPartMD pin_part_md;
            pin_part_md.pin_name = md_map.first;
            pin_part_md.partition_idx = std::distance(md_map.second.begin(),min_elem_itr);
            pin_part_md.md = *min_elem_itr;
            pin_partition_mds.push_back(pin_part_md);
        }//end manhattan_distances_map
        enableOutput = true;
        if(enableOutput){
          std::cout << "candidate pins...\n";
          for(auto pin_part_md : pin_partition_mds){
            std::cout << "pin: " << pin_part_md.pin_name << ", "
                      << "part: " << pin_part_md.partition_idx << ", "
                      << "md: " << pin_part_md.md << std::endl;

          }
        }//end if enableOutput
        enableOutput = false;

        
        std::sort(pin_partition_mds.begin(), pin_partition_mds.end(), 
           [](PinPartMD p1, PinPartMD p2) {return p1.md > p2.md;});

        PinPartMD selected_candidate;
        selected_candidate.pin_name = "-1";
        selected_candidate.partition_idx = -1;
        selected_candidate.md = -1;

        if(pin_partition_mds.size() > 0){
          selected_candidate.pin_name = pin_partition_mds[0].pin_name;
          selected_candidate.partition_idx = pin_partition_mds[0].partition_idx;
          selected_candidate.md = pin_partition_mds[0].md;
        }//end if pin_partition_mds.size() > 0

        //find partition that has maximum area
        if(selected_candidate.pin_name != "-1"){
          auto areas = pin_parts_area_map[selected_candidate.pin_name];
          auto max_elem_itr = std::max_element(areas.begin(),areas.end());          
          int max_elem_idx = std::distance(areas.begin(),max_elem_itr);

           enableOutput = true;
          if(enableOutput){
            std::cout << "pin name: " << selected_candidate.pin_name << std::endl;
            std::cout << "max_elem_idx: " << max_elem_idx << std::endl;
            std::cout << "selected_candidate.partition_idx: " 
                      << selected_candidate.partition_idx << std::endl;
            
          }//end if 
           enableOutput = false;

          if(max_elem_idx != selected_candidate.partition_idx){
            std::string normal_rotation = rotation_table_normal[selected_candidate.partition_idx][max_elem_idx];
            std::string flipped_rotation = rotation_table_flipped[selected_candidate.partition_idx][max_elem_idx];
            enableOutput = true;
            if(flipped_rotation == "frcMY" || flipped_rotation == "frcMX"){
              if(enableOutput){
                std::cout << "inst: " << inst->getName() << std::endl;
                std::cout << "normal roation: " << normal_rotation << std::endl;
                std::cout << "flipped_rotation roation: " << flipped_rotation << std::endl;
              }
              
              frOrient candidate_orient(flipped_rotation);
              // if(inst->getName() == "inst8642")
              inst->setOrient(candidate_orient);
            }
            enableOutput = false;

          }//end if 

        }//end if 
        



        if(enableOutput){
          std::cout << "candidate pins sorted...\n";
          for(auto pin_part_md : pin_partition_mds){
            std::cout << "pin: " << pin_part_md.pin_name << ", "
                      << "part: " << pin_part_md.partition_idx << ", "
                      << "md: " << pin_part_md.md << std::endl;

          }
        }//end if enableOutput


      }//end if valid dist 
      
      
    }//end if 
  }//end for instances 

    


  //   auto ref_block = inst->getRefBlock();
  //   if(ref_block->getName() == "AOI22X1"){
  //     // fanout
  //     auto& terms = ref_block->getTerms();
  //     auto& nets = inst->getNets();
  //     for(auto& net : nets){
  //       if(enableOutput){
  //         std::cout << "net: " << net->getName() << std::endl;
  //         std::cout << "number of connection: " << net->getInstTerms().size() << std::endl;
  //       } 
        
  //       auto& inst_terms = net->getInstTerms();
  //       long int manhattan_distance = 0;
  //       for(auto& inst_term : inst_terms){
  //         auto& inst_connected = inst_term->getInst();
  //         frBox inst_connected_box;
  //         inst_connected->getBBox(inst_connected_box);
  //         auto width_inst = inst_connected_box.width();
  //         auto length_inst = inst_connected_box.length();
  //         frPoint inst_connected_center(inst_connected->getOrigin().x() + (width_inst/2.0),
  //                                       inst_connected->getOrigin().y() + (length_inst/2.0));
  //         manhattan_distance += manhattanDistance()
  //         if(enableOutput){
  //           std::cout << "inst_term: " << inst_term->getInst()->getName() << std::endl;
  //           std::cout << "inst_pin_name: " << inst_term->getTerm()->getName() << std::endl;
  //         } 
          
  //       }//end for 
  //     }
  //     int fan_out = 0;
  //     int fan_in = 0;
  //     for(auto& term : terms){
  //       if(enableOutput) std::cout << "term: " << term->getName() << std::endl;
  //       if(term->getName() == "Y"){
  //         auto net_out = term->getNet();
  //         // if(enableOutput) std::cout << "fanout: " << net_out->getName() << std::endl;
  //       }
  //     }

  //     // fanin 
  //   }
  // }

}//end CellFanOutIn

void FlexDR::cellDensity(){
    // calculate total cell density 
    frBox dieBox2;
    getDesign()->getTopBlock()->getBoundaryBBox(dieBox2);
    std::cout << "die Box length: " << dieBox2.length()/2000.0<< std::endl;
    std::cout << "die Box width: " << dieBox2.width()/2000.0 << std::endl;
    double circuit_area_um = (dieBox2.length()/2000.0) * (dieBox2.width()/2000.0);
    auto& layers = getDesign()->getTech()->getLayers();

    double circuit_volume_um = circuit_area_um*layers.size();
    // double circuit_volume_um = circuit_area_um*3;

    double cell_area_um = 0;
    auto& instances = getDesign()->getTopBlock()->getInsts();
    for(auto& inst : instances){
      frBox inst_box;
      inst->getBBox(inst_box);
      cell_area_um = cell_area_um + (inst_box.width()/2000.0* inst_box.length()/2000.0);

    }//end for getInstances
    std::cout << "total circuit area: " << circuit_area_um << std::endl;
    std::cout << "total cell area: " << cell_area_um << std::endl;
    double cell_density_2D = cell_area_um/circuit_area_um;
    double cell_density_3D = cell_area_um/circuit_volume_um;
    std::cout << "total cell density: " << cell_density_2D << " 2D " <<std::endl;
    std::cout << "total cell density: " << cell_density_3D << " 3D " <<std::endl;

}//end cellDensity 

void FlexDR::netDensity(){
  frBox dieBox2;
  getDesign()->getTopBlock()->getBoundaryBBox(dieBox2);
  std::cout << "die Box length: " << dieBox2.length()/2000.0<< std::endl;
  std::cout << "die Box width: " << dieBox2.width()/2000.0 << std::endl;
  double circuit_area_um = (dieBox2.length()/2000.0) * (dieBox2.width()/2000.0);
  auto& layers = getDesign()->getTech()->getLayers();

  double circuit_volume_um = circuit_area_um*layers.size();


  double wire_area_um = 0;
  double via_area_um = 0;
  double patch_wire_area_um = 0;
  double guide_area_um = 0;

  auto& nets = getDesign()->getTopBlock()->getNets();
  for(auto& net : nets){
    std::cout << "net: " << net->getName() << std::endl;
    auto& shapes = net->getShapes();
    for(auto& shape_ref : shapes){
      auto shape = shape_ref.get();
      frBox wire_box;
      shape->getBBox(wire_box);
      std::cout << "wire box: " << wire_box.lowerLeft().x() << ", "
                                << wire_box.lowerLeft().y() << ", "
                                << wire_box.upperRight().x() << ", "
                                << wire_box.upperRight().y() << "\n";
      wire_area_um = wire_area_um + ((wire_box.length()/2000.0)*(wire_box.width()/2000.0));
    }//end for shapes

    auto& vias = net->getVias();
    for(auto& via_ref : vias){
      frBox via_box;
      via_ref->getBBox(via_box);
      via_area_um = via_area_um + ((via_box.length()/2000.0)*(via_box.width()/2000.0));
      
    }//end for 

    auto& patch_wires = net->getPatchWires();
    for(auto& patch_wire : patch_wires){
      frBox patch_wire_box;
      patch_wire->getBBox(patch_wire_box);
      patch_wire_area_um = patch_wire_area_um + ((patch_wire_box.length()/2000.0)*(patch_wire_box.width()/2000.0));
      
    }//end for 

    auto& guides = net->getGuides();
    for(auto& guide : guides){
      frBox guide_box;
      guide->getBBox(guide_box);
      guide_area_um = guide_area_um + ((guide_box.length()/2000.0)*(guide_box.width()/2000.0));
      
    }//end for 
  }//end for nets 
  std::cout << "total wire area: " << wire_area_um << std::endl;
  double wire_density_3D = wire_area_um/circuit_volume_um;
  std::cout << "total wire density: " << wire_density_3D << " 3D " <<std::endl;

  std::cout << "total via area: " << via_area_um << std::endl;
  double via_density_3D = via_area_um/circuit_volume_um;
  std::cout << "total via density: " << via_density_3D << " 3D " <<std::endl;

  std::cout << "total patch_Wire area: " << patch_wire_area_um << std::endl;
  double patch_wire_density_3D = patch_wire_area_um/circuit_volume_um;
  std::cout << "total patch_wire density: " << patch_wire_density_3D << " 3D " <<std::endl;

  std::cout << "total guide area: " << guide_area_um << std::endl;
  double guide_density_3D = guide_area_um/circuit_volume_um;
  std::cout << "total guide density: " << guide_density_3D << " 3D " <<std::endl;

}//end netDensity 
void FlexDR::blkDensity(){
  auto& instances = getDesign()->getTopBlock()->getInsts();
  frBox dieBox2;
  getDesign()->getTopBlock()->getBoundaryBBox(dieBox2);
  std::cout << "die Box length: " << dieBox2.length()/2000.0<< std::endl;
  std::cout << "die Box width: " << dieBox2.width()/2000.0 << std::endl;
  double circuit_area_um = (dieBox2.length()/2000.0) * (dieBox2.width()/2000.0);
  auto& layers = getDesign()->getTech()->getLayers();
  std::unordered_map<std::string,int> blks_dict;

  auto& blocks = getDesign()->getRefBlocks();

  std::cout << "number of blks: " << blocks.size() << std::endl;

  for(auto& block_ref : blocks){
    auto blk = block_ref.get();
    // std::cout << "blk: " << blk->getName() << std::endl;
    blks_dict[blk->getName()] = 0;
  }

  for(auto& inst : instances){
    const auto& blk_ref = inst->getRefBlock();
    if(blks_dict.find(blk_ref->getName()) != blks_dict.end()){
      blks_dict[blk_ref->getName()] +=1;
    }else{
      blks_dict[blk_ref->getName()] = 1;
    }
  }//end for getInstances

  ofstream feature_extraction_file_blks;
  feature_extraction_file_blks.open("feature_extraction_blks.txt");
  feature_extraction_file_blks << "blk,num\n";
  for(auto itr : blks_dict){
    feature_extraction_file_blks << itr.first << ", " << itr.second << std::endl;
  }
  feature_extraction_file_blks.close();
}//end blkDensity 

void FlexDR::writeFeatureExtraction(vector<vector<vector<unique_ptr<FlexDRWorker> > > >& workers){
  ofstream feature_extraction_file;
  feature_extraction_file.open("feature_extraction.txt");

  feature_extraction_file << "i" << "," << "j" << "," 
                      << "box_left_x" << ","
                      << "box_left_y" << ","
                      << "box_right_x" << ","
                      << "box_right_y" << ","
                      << "num_wires" << ","
                      << "num_vias" << ","
                      << "num_patches" << ","
                      << "wl_of_box" << ","
                      << "num_instances" << std::endl;

  //added by erfan for feature extraction
  for (auto &workerBatch: workers) {
      for (auto &workersInBatch: workerBatch) {
        for (int i = 0; i < (int)workersInBatch.size(); i++) {
          // workersInBatch[i]->main_mt();
          auto feature_extraction = workersInBatch[i]->m_feature_extraction;
          frBox route_box,ext_box,drc_box; 
          workersInBatch[i]->getRouteBox(route_box);
          workersInBatch[i]->getRouteBox(ext_box);
          workersInBatch[i]->getRouteBox(drc_box);
          int i_idx = workersInBatch[i]->getI();
          int j_idx = workersInBatch[i]->getJ();

          feature_extraction_file << i_idx << "," << j_idx << "," 
                                  << feature_extraction.m_route_box.lowerLeft().x() << ","
                                  << feature_extraction.m_route_box.lowerLeft().y() << ","
                                  << feature_extraction.m_route_box.upperRight().x() << ","
                                  << feature_extraction.m_route_box.upperRight().y() << ","
                                  << feature_extraction.m_number_of_wires << ","
                                  << feature_extraction.m_number_of_vias << ","
                                  << feature_extraction.m_number_of_patches << ","
                                  << feature_extraction.m_wl_of_worker << ","
                                  << feature_extraction.m_instances_name.size() << std::endl;
                                  


          // std::string inst_string = "";
                  
          // for(auto obj : feature_extraction.m_instances){
          //     if (obj->typeId() == frcInstTerm) {
          //         auto instTerm = static_cast<frInstTerm*>(obj);        
          //         frBox inst_box;
          //         instTerm->getInst()->getBBox(inst_box);
          //         long long area = inst_box.length() * inst_box.width();

          //         std::cout << "inst: " << instTerm->getInst()->getName() << ", "
          //                   << "Box: "  << inst_box.lowerLeft().x() /2000 << ", " << 
          //                                  inst_box.lowerLeft().y() /2000 << ", " <<
          //                                  inst_box.upperRight().x()/2000 << ", " <<
          //                                  inst_box.upperRight().y()/2000 << ", area: " << area <<std::endl;
                  

          //     }//end if
          // }//end for 

          // std::cout << "inst_name size: " << feature_extraction.m_instances_name.size() << std::endl;
          // for(auto inst_name : feature_extraction.m_instances_name){
          //   std::cout << "inst_name: " << inst_name << std::endl;
          // }

          // std::cout << "inst type size: " << feature_extraction.m_instances_type.size() << std::endl;
          // for(auto inst_type : feature_extraction.m_instances_type){
          //   std::cout << "inst_type: " << inst_type << std::endl;
          // }
      
          
          // std::cout << "m_number_of_cells_route_box: " << feature_extraction.m_number_of_cells_route_box << std::endl;
          // std::cout << "m_number_of_cells_ext_box: "   << feature_extraction.m_number_of_cells_ext_box << std::endl;
          // std::cout << "m_number_of_cells_drc_box: "   << feature_extraction.m_number_of_cells_drc_box << std::endl;
          
          // std::cout << "m_region_density_route_box: " << feature_extraction.m_region_density_route_box << std::endl;
          // std::cout << "m_region_density_ext_box: " << feature_extraction.m_region_density_ext_box << std::endl;
          // std::cout << "m_region_density_drc_box: " << feature_extraction.m_region_density_drc_box << std::endl;

        }
      }
  }//end for 

  feature_extraction_file.close();

}//end writeFeatureExtraction

void FlexDR::applyMovement(){
  auto orientStr = [](int orient){
    switch (orient) {
      case 0: return ("N");
      case 1: return ("W");
      case 2: return ("S");
      case 3: return ("E");
      case 4: return ("FN");
      case 5: return ("FW");
      case 6: return ("FS");
      case 7: return ("FE");
    };
    return "BOGUS";
  };


  auto getNetBox = [](frBox& box,frNet* net){
      auto& inst_terms = net->getInstTerms();
       
      std::vector<frCoord> x_s;
      std::vector<frCoord> y_s;
      for(auto& inst_term : inst_terms){
        frBox inst_box;
        inst_term->getInst()->getBBox(inst_box);
        x_s.push_back(inst_box.left());
        x_s.push_back(inst_box.right());
        y_s.push_back(inst_box.bottom());
        y_s.push_back(inst_box.top());
      }
      auto xl_itr = std::min_element(x_s.begin(),x_s.end());
      auto xh_itr = std::max_element(x_s.begin(),x_s.end());
      auto yl_itr = std::min_element(y_s.begin(),y_s.end());
      auto yh_itr = std::max_element(y_s.begin(),y_s.end());
      
      box.set(*xl_itr,*yl_itr,*xh_itr,*yh_itr);
    
  };//end lambda function 
  
  


  auto& instances = getDesign()->getTopBlock()->getInsts();
  std::cout << "apply movement ... \n";
  for(auto& inst : instances ){
    frPoint inst_origin;
    inst->getOrigin(inst_origin);
    // std::cout << "inst: " << inst->getName() << ","
    //                       << inst_origin.x() <<", " << inst_origin.y() << std::endl;

    std::cout << "inst: " << inst->getName() << std::endl;
    if(inst->getName() == "inst8879"){
      // frOrient orient = 
      // auto orient = inst->getOrient();
      auto orient = inst->getOrient();
      std::cout << "before rotation: " << std::endl;

      auto&  terms = inst->getInstTerms();

      for(auto& term : terms){
        auto& pins = term->getTerm()->getPins();
        for(auto& pin : pins ){
          auto& figs = pin->getFigs();
          for(auto& fig : figs){
            frBox fig_box;
            fig->getBBox(fig_box);
            std::cout << "before fig_box: " << fig_box.lowerLeft().x() << ", "
                                            << fig_box.lowerLeft().y() << ", "
                                            << fig_box.upperRight().x() << ", "
                                            << fig_box.upperRight().y() << "\n";

          }//end for 
          
        }//end for 
        
      }//end of term 



      frOrient new_orient = frcMY;
      std::cout << "current orient is: " << orientStr(frOrientEnum(orient)) << std::endl;
      inst->setOrient(new_orient);


      // update pinFigs
      std::cout << "update pinFigs: ...\n";






      std::cout << "after rotation: " << std::endl;


      for(auto& term : terms){
        auto& pins = term->getTerm()->getPins();
        for(auto& pin : pins ){
          auto& figs = pin->getFigs();
          for(auto& fig : figs){
            frBox fig_box;
            fig->getBBox(fig_box);
            std::cout << "after fig_box: " << fig_box.lowerLeft().x() << ", "
                                            << fig_box.lowerLeft().y() << ", "
                                            << fig_box.upperRight().x() << ", "
                                            << fig_box.upperRight().y() << "\n";

          }//end for 
          
        }//end for 
        
      }//end of term 

      auto& nets = inst->getNets();
      auto query_region = getRegionQuery();
      for(auto& net : nets){
        //get vias
        for(auto& shape : net->getShapes()){
          query_region->removeDRObj(static_cast<frShape*>(shape.get()));
        }
        for(auto& via : net->getVias()){
          query_region->removeDRObj(static_cast<frVia*>(via.get()));
        }
        // net->cleanup();
        
        FlexPA pa(getDesign());
        pa.main();
        // auto& vias = net->getVias();
        // std::cout << "number of vias is: " << vias.size()<<std::endl;
        // for(auto& via_ref:vias ){
        //   net->removeVia(via_ref.get());
        // }

        std::cout << "net of inst: " << net->getName() << std::endl;
        frBox net_box;

        getNetBox(net_box,net);
        

        std::cout << "net_box: " << net_box.lowerLeft().x() << ", "
                                 << net_box.lowerLeft().y() << ", "
                                 << net_box.upperRight().x() << ", "
                                 << net_box.upperRight().y() << "\n";
        

        // route net again

        auto& insts_of_net = net->getInstTerms();
        for(auto& inst_of_net : insts_of_net){
          std::cout << "inst_of_net: " << inst_of_net->getInst()->getName() << std::endl;
        }
        
        // init();

        // bool enableOut = true;
        // if(enableOut) std::cout << "routeNet...\n";
        FlexDRWorker worker(this);
        
        
        frBox extBox;
        frBox drcBox;
        int mazeEndItr = 1;

        
        // getDesign()->getTopBlock()->getBoundaryBBox(net_box);

        net_box.bloat(2000,extBox);
        net_box.bloat(500,drcBox);


        worker.setRouteBox(net_box);
        worker.setExtBox(extBox);
        worker.setDrcBox(drcBox);
        
        worker.setMazeEndIter(mazeEndItr);

        auto route_box = worker.getRouteBox();
        std::cout << "the route box should be: " 
                                 << route_box.lowerLeft().x() << ", "
                                 << route_box.lowerLeft().y() << ", "
                                 << route_box.upperRight().x() << ", "
                                 << route_box.upperRight().y() << "\n";

        
        
        
        worker.main();

        std::cout << "done!" << std::endl;

      }//end if 
      // auto& blks_ref = getDesign()->getRefBlocks();
      

      

            
        
      // auto& blks = inst->getInstBlockages();
      // for(auto blk_ref : )
      // std::cout << "inst net: " << net->getName() << std::endl;
    }else{
      std::cout << "nope\n";
    }//end if 




    // auto& terms = inst->getInstTerms();
    // for(auto& term : terms){
    //   const auto& net = term->getNet();
    //   std::cout << "term: " << term->getTerm()->getName() << ", net: " << net->getName() << std::endl;
    // }//end for terms
                          
  }//end instance 




  // for(auto& net : getDesign()->getTopBlock()->getNets()){
  //   for(auto instTerm : net->getInstTerms()){
  //       auto inst = instTerm->getInst();
  //       std::cout << inst->getName() << std::endl; 
  //       if(inst->getName() == "inst5638"){
  //         frPoint point;
  //         inst->getOrigin(point);
  //         std::cout << "before move: " << point.x() <<", " << point.y() << std::endl;
  //         frPoint new_point(point.x() + 1000 , point.y() + 1000);
  //         inst->setOrigin(new_point);
  //         frPoint point2;
  //         inst->getOrigin(point2);
  //         std::cout << "after move: " << point2.x() <<", " << point2.y() << std::endl;
  //       }//end if 
  //     }//end for 
  // }//end for 
     
}//end applyMovement

void FlexDR::end() {
  vector<unsigned long long> wlen(getTech()->getLayers().size(), 0);
  vector<unsigned long long> sCut(getTech()->getLayers().size(), 0);
  vector<unsigned long long> mCut(getTech()->getLayers().size(), 0);
  unsigned long long totWlen = 0;
  unsigned long long totSCut = 0;
  unsigned long long totMCut = 0;
  frPoint bp, ep;
  for (auto &net: getDesign()->getTopBlock()->getNets()) {
    for (auto &shape: net->getShapes()) {
      if (shape->typeId() == frcPathSeg) {
        auto obj = static_cast<frPathSeg*>(shape.get());
        obj->getPoints(bp, ep);
        auto lNum = obj->getLayerNum();
        frCoord psLen = ep.x() - bp.x() + ep.y() - bp.y();
        wlen[lNum] += psLen;
        totWlen += psLen;
      }
    }
    for (auto &via: net->getVias()) {
      auto lNum = via->getViaDef()->getCutLayerNum();
      if (via->getViaDef()->isMultiCut()) {
        ++mCut[lNum];
        ++totMCut;
      } else {
        ++sCut[lNum];
        ++totSCut;
      }
    }
  }
  if (VERBOSE > 0) {
    boost::io::ios_all_saver guard(std::cout);
    cout <<endl <<"total wire length = " <<totWlen / getDesign()->getTopBlock()->getDBUPerUU() <<" um" <<endl;
    for (int i = getTech()->getBottomLayerNum(); i <= getTech()->getTopLayerNum(); i++) {
      if (getTech()->getLayer(i)->getType() == frLayerTypeEnum::ROUTING) {
        cout <<"total wire length on LAYER " <<getTech()->getLayer(i)->getName() <<" = " 
             <<wlen[i] / getDesign()->getTopBlock()->getDBUPerUU() <<" um" <<endl;
      }
    }
    cout <<"total number of vias = " <<totSCut + totMCut <<endl;
    if (totMCut > 0) {
      cout <<"total number of multi-cut vias = " <<totMCut 
           << " (" <<setw(5) <<fixed <<setprecision(1) <<totMCut * 100.0 / (totSCut + totMCut) <<"%)" <<endl;
      cout <<"total number of single-cut vias = " <<totSCut 
           << " (" <<setw(5) <<fixed <<setprecision(1) <<totSCut * 100.0 / (totSCut + totMCut) <<"%)" <<endl;
    }
    cout <<"up-via summary (total " <<totSCut + totMCut <<"):" <<endl;
    int nameLen = 0;
    for (int i = getTech()->getBottomLayerNum(); i <= getTech()->getTopLayerNum(); i++) {
      if (getTech()->getLayer(i)->getType() == frLayerTypeEnum::CUT) {
        nameLen = max(nameLen, (int)getTech()->getLayer(i-1)->getName().size());
      }
    }
    int maxL = 1 + nameLen + 4 + (int)to_string(totSCut).length();
    if (totMCut) {
      maxL += 9 + 4 + (int)to_string(totMCut).length() + 9 + 4 + (int)to_string(totSCut + totMCut).length();
    }
    if (totMCut) {
      cout <<" " <<setw(nameLen + 4 + (int)to_string(totSCut).length() + 9) <<"single-cut";
      cout <<setw(4 + (int)to_string(totMCut).length() + 9) <<"multi-cut" 
           <<setw(4 + (int)to_string(totSCut + totMCut).length()) <<"total";
    }
    cout <<endl;
    for (int i = 0; i < maxL; i++) {
      cout <<"-";
    }
    cout <<endl;
    for (int i = getTech()->getBottomLayerNum(); i <= getTech()->getTopLayerNum(); i++) {
      if (getTech()->getLayer(i)->getType() == frLayerTypeEnum::CUT) {
        cout <<" "    <<setw(nameLen) <<getTech()->getLayer(i-1)->getName() 
             <<"    " <<setw((int)to_string(totSCut).length()) <<sCut[i];
        if (totMCut) {
          cout <<" ("   <<setw(5) <<(double)((sCut[i] + mCut[i]) ? sCut[i] * 100.0 / (sCut[i] + mCut[i]) : 0.0) <<"%)";
          cout <<"    " <<setw((int)to_string(totMCut).length()) <<mCut[i] 
               <<" ("   <<setw(5) <<(double)((sCut[i] + mCut[i]) ? mCut[i] * 100.0 / (sCut[i] + mCut[i]) : 0.0) <<"%)"
               <<"    " <<setw((int)to_string(totSCut + totMCut).length()) <<sCut[i] + mCut[i];
        }
        cout <<endl;
      }
    }
    for (int i = 0; i < maxL; i++) {
      cout <<"-";
    }
    cout <<endl;
    cout <<" "    <<setw(nameLen) <<""
         <<"    " <<setw((int)to_string(totSCut).length()) <<totSCut;
    if (totMCut) {
      cout <<" ("   <<setw(5) <<(double)((totSCut + totMCut) ? totSCut * 100.0 / (totSCut + totMCut) : 0.0) <<"%)";
      cout <<"    " <<setw((int)to_string(totMCut).length()) <<totMCut 
           <<" ("   <<setw(5) <<(double)((totSCut + totMCut) ? totMCut * 100.0 / (totSCut + totMCut) : 0.0) <<"%)"
           <<"    " <<setw((int)to_string(totSCut + totMCut).length()) <<totSCut + totMCut;
    }
    cout <<endl <<endl <<flush;
    guard.restore();
  }
}

void FlexDR::reportDRC() {
  double dbu = design->getTech()->getDBUPerUU();

  if (DRC_RPT_FILE == string("")) {
    if (VERBOSE > 0) {
      cout <<"Waring: no DRC report specified, skipped writing DRC report" <<endl;
    }
    return;
  }
  // cout << "DRC_RPT_FILE: " << DRC_RPT_FILE << "\n";
  std::cout << "number of markers: " << getDesign()->getTopBlock()->getNumMarkers() << std::endl;
  ofstream drcRpt(DRC_RPT_FILE.c_str());
  if (drcRpt.is_open()) {
    
    for (auto &marker: getDesign()->getTopBlock()->getMarkers()) {

      std::cout << "drcRpt getDesign()->getTopBlock()!" << std::endl;
      auto con = marker->getConstraint();
      drcRpt << "  violation type: ";
      if (con) {
        if (con->typeId() == frConstraintTypeEnum::frcShortConstraint) {
          if (getTech()->getLayer(marker->getLayerNum())->getType() == frLayerTypeEnum::ROUTING) {
            drcRpt <<"Short";
          } else if (getTech()->getLayer(marker->getLayerNum())->getType() == frLayerTypeEnum::CUT) {
            drcRpt <<"CShort";
          }
        } else if (con->typeId() == frConstraintTypeEnum::frcMinWidthConstraint) {
          drcRpt <<"MinWid";
        } else if (con->typeId() == frConstraintTypeEnum::frcSpacingConstraint) {
          drcRpt <<"MetSpc";
        } else if (con->typeId() == frConstraintTypeEnum::frcSpacingEndOfLineConstraint) {
          drcRpt <<"EOLSpc";
        } else if (con->typeId() == frConstraintTypeEnum::frcSpacingTablePrlConstraint) {
          drcRpt <<"MetSpc";
        } else if (con->typeId() == frConstraintTypeEnum::frcCutSpacingConstraint) {
          drcRpt <<"CutSpc";
        } else if (con->typeId() == frConstraintTypeEnum::frcMinStepConstraint) {
          drcRpt <<"MinStp";
        } else if (con->typeId() == frConstraintTypeEnum::frcNonSufficientMetalConstraint) {
          drcRpt <<"NSMet";
        } else if (con->typeId() == frConstraintTypeEnum::frcSpacingSamenetConstraint) {
          drcRpt <<"MetSpc";
        } else if (con->typeId() == frConstraintTypeEnum::frcOffGridConstraint) {
          drcRpt <<"OffGrid";
        } else if (con->typeId() == frConstraintTypeEnum::frcMinEnclosedAreaConstraint) {
          drcRpt <<"MinHole";
        } else if (con->typeId() == frConstraintTypeEnum::frcAreaConstraint) {
          drcRpt <<"MinArea";
        } else if (con->typeId() == frConstraintTypeEnum::frcLef58CornerSpacingConstraint) {
          drcRpt <<"CornerSpc";
        } else if (con->typeId() == frConstraintTypeEnum::frcLef58CutSpacingConstraint) {
          drcRpt <<"CutSpc";
        } else if (con->typeId() == frConstraintTypeEnum::frcLef58RectOnlyConstraint) {
          drcRpt <<"RectOnly";
        } else if (con->typeId() == frConstraintTypeEnum::frcLef58RightWayOnGridOnlyConstraint) {
          drcRpt <<"RightWayOnGridOnly";
        } else if (con->typeId() == frConstraintTypeEnum::frcLef58MinStepConstraint) {
          drcRpt <<"MinStp";
        } else {
          drcRpt << "unknown";
        }
      } else {
        drcRpt << "nullptr";
      }
      drcRpt <<endl;
      // get source(s) of violation
      drcRpt << "    srcs: ";
      for (auto src: marker->getSrcs()) {
        if (src) {
          switch (src->typeId()) {
            case frcNet:
              drcRpt << (static_cast<frNet*>(src))->getName() << " ";
              break;
            case frcInstTerm: {
              frInstTerm* instTerm = (static_cast<frInstTerm*>(src));
              drcRpt <<instTerm->getInst()->getName() <<"/" <<instTerm->getTerm()->getName() << " ";
              break;
            }
            case frcTerm: {
              frTerm* term = (static_cast<frTerm*>(src));
              drcRpt <<"PIN/" << term->getName() << " ";
              break;
            }
            case frcInstBlockage: {
              frInstBlockage* instBlockage = (static_cast<frInstBlockage*>(src));
              drcRpt <<instBlockage->getInst()->getName() <<"/OBS" << " ";
              break;
            }
            case frcBlockage: {
              drcRpt << "PIN/OBS" << " ";
              break;
            }
            default:
              std::cout << "Error: unexpected src type in marker\n";
          }
        }
      }
      drcRpt << "\n";
      // get violation bbox
      frBox bbox;
      marker->getBBox(bbox);
      drcRpt << "    bbox = ( " << bbox.left() / dbu << ", " << bbox.bottom() / dbu << " ) - ( "
             << bbox.right() / dbu << ", " << bbox.top() / dbu << " ) on Layer ";
      if (getTech()->getLayer(marker->getLayerNum())->getType() == frLayerTypeEnum::CUT && 
          marker->getLayerNum() - 1 >= getTech()->getBottomLayerNum()) {
        drcRpt << getTech()->getLayer(marker->getLayerNum() - 1)->getName() << "\n";
      } else {
        drcRpt << getTech()->getLayer(marker->getLayerNum())->getName() << "\n";
      }
    }
  } else {
    cout << "Error: Fail to open DRC report file\n";
  }
  
}


int FlexDR::main() {
  init();
  frTime t;
  if (VERBOSE > 0) {
    cout <<endl <<endl <<"start detail routing ...";
  }
  // search and repair: iter, size, offset, mazeEndIter, workerDRCCost, workerMarkerCost, 
  //                    markerBloatWidth, markerBloatDepth, enableDRC, ripupMode, followGuide, fixMode, TEST
  // fixMode:
  //   0 - general fix
  //   1 - fat via short, spc to wire fix (keep via net), no permutation, increasing DRCCOST
  //   2 - fat via short, spc to wire fix (ripup via net), no permutation, increasing DRCCOST
  //   3 - general fix, ripup everything (bloat)
  //   4 - general fix, ripup left/bottom net (touching), currently DISABLED
  //   5 - general fix, ripup right/top net (touching), currently DISABLED
  //   6 - two-net viol
  //   9 - search-and-repair queue
  // assume only mazeEndIter > 1 if enableDRC and ripupMode == 0 (partial ripup)
  //end();
  //searchRepair(1,  7, -4,  1, DRCCOST, 0,          0, 0, true, 1, false, 0, true); // test mode

  // need three different offsets to resolve boundary corner issues


  // searchRepair(int iter,  int size, int offset, int mazeEndIter, frUInt4 workerDRCCost,  frUInt4 workerMarkerCost, frUInt4 workerMarkerBloatWidth, frUInt4 workerMarkerBloatDepth,
  // searchRepair(iterNum ,  7       , 0         , 3              , DRCCOST              ,  0/*MAARKERCOST*/        ,  0                            , 0                             , 

  // bool enableDRC, int ripupMode, bool followGuide, int fixMode,
  // true          , 1            , true            , 9); // true search and repair

  int iterNum = 0;
  searchRepair(iterNum++/*  0 */,  7,  0, 3, DRCCOST, 0/*MAARKERCOST*/,  0, 0, true, 1, true, 9); // true search and repair
  // searchRepair(iterNum++/*  1 */,  7, -2, 3, DRCCOST, DRCCOST/*MAARKERCOST*/,  0, 0, true, 1, true, 9); // true search and repair
  // searchRepair(iterNum++/*  1 */,  7, -5, 3, DRCCOST, DRCCOST/*MAARKERCOST*/,  0, 0, true, 1, true, 9); // true search and repair
  // searchRepair(iterNum++/*  3 */,  7,  0, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  4 */,  7, -1, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  5 */,  7, -2, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  6 */,  7, -3, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  7 */,  7, -4, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  8 */,  7, -5, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  9 */,  7, -6, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 10 */,  7,  0, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 11 */,  7, -1, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 12 */,  7, -2, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 13 */,  7, -3, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 14 */,  7, -4, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 15 */,  7, -5, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 16 */,  7, -6, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  7, -3, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 17 */,  7,  0, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 18 */,  7, -1, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 19 */,  7, -2, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 20 */,  7, -3, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 21 */,  7, -4, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 22 */,  7, -5, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 23 */,  7, -6, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  5, -2, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 24 */,  7,  0, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 25 */,  7, -1, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 26 */,  7, -2, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 27 */,  7, -3, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 28 */,  7, -4, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 29 */,  7, -5, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 30 */,  7, -6, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  3, -1, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 31 */,  7,  0, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 32 */,  7, -1, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 33 */,  7, -2, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 34 */,  7, -3, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 35 */,  7, -4, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 36 */,  7, -5, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 37 */,  7, -6, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  3, -2, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 38 */,  7,  0, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 39 */,  7, -1, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 40 */,  7, -2, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 41 */,  7, -3, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 42 */,  7, -4, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 43 */,  7, -5, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 44 */,  7, -6, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  3, -0, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 45 */,  7,  0, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 46 */,  7, -1, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 47 */,  7, -2, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 48 */,  7, -3, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 49 */,  7, -4, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 50 */,  7, -5, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 51 */,  7, -6, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  3, -1, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 52 */,  7,  0, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 53 */,  7, -1, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 54 */,  7, -2, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 55 */,  7, -3, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 56 */,  7, -4, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 57 */,  7, -5, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 58 */,  7, -6, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair

  
  if (DRC_RPT_FILE != string("")) {
    reportDRC();
  }
  if (VERBOSE > 0) {
    cout <<endl <<"complete detail routing";
    end();
  }
  if (VERBOSE > 0) {
    t.print();
    cout <<endl;
  }
  return 0;
}




int FlexDR::mainFeatureExtraction() {
  init();
  frTime t;
  if (VERBOSE > 0) {
    cout <<endl <<endl <<"start detail routing ...";
  }
  // search and repair: iter, size, offset, mazeEndIter, workerDRCCost, workerMarkerCost, 
  //                    markerBloatWidth, markerBloatDepth, enableDRC, ripupMode, followGuide, fixMode, TEST
  // fixMode:
  //   0 - general fix
  //   1 - fat via short, spc to wire fix (keep via net), no permutation, increasing DRCCOST
  //   2 - fat via short, spc to wire fix (ripup via net), no permutation, increasing DRCCOST
  //   3 - general fix, ripup everything (bloat)
  //   4 - general fix, ripup left/bottom net (touching), currently DISABLED
  //   5 - general fix, ripup right/top net (touching), currently DISABLED
  //   6 - two-net viol
  //   9 - search-and-repair queue
  // assume only mazeEndIter > 1 if enableDRC and ripupMode == 0 (partial ripup)
  //end();
  //searchRepair(1,  7, -4,  1, DRCCOST, 0,          0, 0, true, 1, false, 0, true); // test mode

  // need three different offsets to resolve boundary corner issues


  // searchRepair(int iter,  int size, int offset, int mazeEndIter, frUInt4 workerDRCCost,  frUInt4 workerMarkerCost, frUInt4 workerMarkerBloatWidth, frUInt4 workerMarkerBloatDepth,
  // searchRepair(iterNum ,  7       , 0         , 3              , DRCCOST              ,  0/*MAARKERCOST*/        ,  0                            , 0                             , 

  // bool enableDRC, int ripupMode, bool followGuide, int fixMode,
  // true          , 1            , true            , 9); // true search and repair

  int iterNum = 0;
  searchFeatureExtraction(iterNum++/*  0 */,  7,  0, 3, DRCCOST, 0/*MAARKERCOST*/,  0, 0, true, 1, true, 9); // true search and repair
  // searchRepair(iterNum++/*  1 */,  7, -2, 3, DRCCOST, DRCCOST/*MAARKERCOST*/,  0, 0, true, 1, true, 9); // true search and repair
  // searchRepair(iterNum++/*  1 */,  7, -5, 3, DRCCOST, DRCCOST/*MAARKERCOST*/,  0, 0, true, 1, true, 9); // true search and repair
  // searchRepair(iterNum++/*  3 */,  7,  0, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  4 */,  7, -1, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  5 */,  7, -2, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  6 */,  7, -3, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  7 */,  7, -4, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  8 */,  7, -5, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/*  9 */,  7, -6, 8, DRCCOST, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 10 */,  7,  0, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 11 */,  7, -1, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 12 */,  7, -2, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 13 */,  7, -3, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 14 */,  7, -4, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 15 */,  7, -5, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 16 */,  7, -6, 8, DRCCOST*2, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  7, -3, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 17 */,  7,  0, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 18 */,  7, -1, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 19 */,  7, -2, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 20 */,  7, -3, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 21 */,  7, -4, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 22 */,  7, -5, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 23 */,  7, -6, 8, DRCCOST*4, MARKERCOST,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  5, -2, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 24 */,  7,  0, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 25 */,  7, -1, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 26 */,  7, -2, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 27 */,  7, -3, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 28 */,  7, -4, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 29 */,  7, -5, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 30 */,  7, -6, 8, DRCCOST*8, MARKERCOST*2,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  3, -1, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 31 */,  7,  0, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 32 */,  7, -1, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 33 */,  7, -2, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 34 */,  7, -3, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 35 */,  7, -4, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 36 */,  7, -5, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 37 */,  7, -6, 8, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  3, -2, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 38 */,  7,  0, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 39 */,  7, -1, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 40 */,  7, -2, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 41 */,  7, -3, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 42 */,  7, -4, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 43 */,  7, -5, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 44 */,  7, -6, 16, DRCCOST*16, MARKERCOST*4,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  3, -0, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 45 */,  7,  0, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 46 */,  7, -1, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 47 */,  7, -2, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 48 */,  7, -3, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 49 */,  7, -4, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 50 */,  7, -5, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 51 */,  7, -6, 32, DRCCOST*32, MARKERCOST*8,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* ra'*/,  3, -1, 8, DRCCOST, MARKERCOST,  0, 0, true, 1, false, 9); // true search and repair
  // searchRepair(iterNum++/* 52 */,  7,  0, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 53 */,  7, -1, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 54 */,  7, -2, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 55 */,  7, -3, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 56 */,  7, -4, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 57 */,  7, -5, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair
  // searchRepair(iterNum++/* 58 */,  7, -6, 64, DRCCOST*64, MARKERCOST*16,  0, 0, true, 0, false, 9); // true search and repair

  
  if (DRC_RPT_FILE != string("")) {
    reportDRC();
  }
  if (VERBOSE > 0) {
    cout <<endl <<"complete detail routing";
    end();
  }
  if (VERBOSE > 0) {
    t.print();
    cout <<endl;
  }
  return 0;
}

void FlexDR::featureExtraction(){
  std::cout << "feature extraction ...\n";
    init();

  // erfan: test move an instance 
  // for(auto& net : getDesign()->getTopBlock()->getNets()){
  //     for(auto instTerm : net->getInstTerms()){
  //       auto inst = instTerm->getInst();
  //       std::cout << inst->getName() << std::endl; 
  //       if(inst->getName() == "inst5638"){
  //         frPoint point;
  //         inst->getOrigin(point);
  //         std::cout << "before move: " << point.x() <<", " << point.y() << std::endl;
  //         frPoint new_point(point.x() + 1000 , point.y() + 1000);
  //         inst->setOrigin(new_point);
  //         frPoint point2;
  //         inst->getOrigin(point2);
  //         std::cout << "after move: " << point2.x() <<", " << point2.y() << std::endl;
  //       }//end if 
  //     }//end for 
  // }//end for 



  bool enableOut = true;
  if(enableOut) std::cout << "routeNet...\n";
  FlexDRWorker worker(this);
  frBox routeBox;
  frBox extBox;
  frBox drcBox;
  int mazeEndItr = 3;

  frBox dieBox;
  getDesign()->getTopBlock()->getBoundaryBBox(dieBox);

  dieBox.bloat(2000,extBox);
  dieBox.bloat(500,drcBox);


  worker.setRouteBox(dieBox);
  worker.setExtBox(extBox);
  worker.setDrcBox(drcBox);
  
  worker.setMazeEndIter(mazeEndItr);
  
  worker.main();

  std::cout << "done!" << std::endl;

}//end featureExtraction


bool FlexDR::routeNet(){
  // searchRepair(int iter,  int size, int offset, int mazeEndIter, frUInt4 workerDRCCost,  frUInt4 workerMarkerCost, frUInt4 workerMarkerBloatWidth, frUInt4 workerMarkerBloatDepth,
  // searchRepair(iterNum ,  7       , 0         , 3              , DRCCOST              ,  0/*MAARKERCOST*/        ,  0                            , 0                             , 

  // bool enableDRC, int ripupMode, bool followGuide, int fixMode,
  // true          , 1            , true            , 9); // true search and repair


  init();

  // erfan: test move an instance 
  for(auto& net : getDesign()->getTopBlock()->getNets()){
      for(auto instTerm : net->getInstTerms()){
        auto inst = instTerm->getInst();
        std::cout << inst->getName() << std::endl; 
        if(inst->getName() == "inst5638"){
          frPoint point;
          inst->getOrigin(point);
          std::cout << "before move: " << point.x() <<", " << point.y() << std::endl;
          frPoint new_point(point.x() + 1000 , point.y() + 1000);
          inst->setOrigin(new_point);
          frPoint point2;
          inst->getOrigin(point2);
          std::cout << "after move: " << point2.x() <<", " << point2.y() << std::endl;
        }//end if 
      }//end for 
  }//end for 



  bool enableOut = true;
  if(enableOut) std::cout << "routeNet...\n";
  FlexDRWorker worker(this);
  frBox routeBox;
  frBox extBox;
  frBox drcBox;
  int mazeEndItr = 3;

  frBox dieBox;
  getDesign()->getTopBlock()->getBoundaryBBox(dieBox);

  dieBox.bloat(2000,extBox);
  dieBox.bloat(500,drcBox);


  worker.setRouteBox(dieBox);
  worker.setExtBox(extBox);
  worker.setDrcBox(drcBox);
  
  worker.setMazeEndIter(mazeEndItr);
  
  worker.main();

  std::cout << "done!" << std::endl;

  return false;

}//end routeNets
