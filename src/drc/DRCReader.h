// Author: erfan aghaeekiasaraee
// email: erfan.aghaeekiasarae@ucalgary.ca
// Comments: read the drc report

#ifndef _DRCREADER_H_
#define _DRCREADER_H_


#include <iostream>
#include <iterator>
#include <string>
#include <regex>
#include <fstream>
#include <boost/algorithm/string.hpp> 

namespace ucal{

class Short{
    public:
        Short(){}
        ~Short(){}
        void setConflitNets(std::vector<std::string>& conflicts){
            m_conflict_nets = conflicts;
        }
        void setConflitTypes(std::vector<std::string>& conflicts){
            m_conflict_types = conflicts;
        }
        void setConflitLayer(std::vector<std::string>& conflicts){
            m_conflict_layer = conflicts;
        }
        std::vector<std::string>& getConflitNets(){
            return m_conflict_nets;
        }
        std::vector<std::string>& getConflitTypes(){
            return m_conflict_types;
        }
        std::vector<std::string>& setConflitLayer(){
            return m_conflict_layer;
        }
    private: 
        std::vector<std::string> m_conflict_nets;
        std::vector<std::string> m_conflict_types;
        std::vector<std::string> m_conflict_layer;

};//end struct

class DRCReader{
public:
    DRCReader(){
        std::string dir = "reports_TR_init_GR/reports/ispd18_test1/";
        m_geo_file = dir + "eval.geo.txt";
        m_con_file = dir + "eval.con.txt";
        m_score_file = dir + "eval.score.txt";
    }
    ~DRCReader(){}

    void run(){
        std::ifstream read_geo_file;
        read_geo_file.open("eval_geo_rpt.txt");
        if (!read_geo_file) {
            std::cout << "Unable to open eval.geo.rpt file";
            exit(1); // terminate with error
        }
        std::string line;
        std::string file_contents = "";
        while (std::getline(read_geo_file, line))
        {
            file_contents = file_contents + line + "\n";
            
        }
        // std::cout << file_contents << std::endl;

        read_geo_file.close();    


        drcSummary(file_contents);  

        drcShorts(file_contents);

    }//end run function

    std::vector<Short>& getShorts(){return m_shorts;}

private:

    void drcShorts(std::string& file_contents){
        bool enableOut = true;
        std::vector<std::string> violation_types = {
            "SHORT"
        };
        std::string pattern = ".*";

        auto drc_reports = parseRegex(file_contents,pattern,violation_types);

        //print reports 
        if(enableOut)
            for(auto drc_report : drc_reports)
                std::cout << drc_report << std::endl;
        for(auto txt_short : drc_reports){
            std::string pattern_conflict_nets = "net\\d+";
            std::string pattern_conflict_types = "Wire|Via";
            std::string pattern_conflict_layer = "Metal\\d+";
            std::vector<std::string> conflict_nets;
            std::vector<std::string> conflict_types;
            std::vector<std::string> conflict_layer;
            conflict_nets = parseRegex(txt_short,pattern_conflict_nets);
            conflict_types = parseRegex(txt_short,pattern_conflict_types);
            conflict_layer = parseRegex(txt_short,pattern_conflict_layer);
            Short short_violation;
            short_violation.setConflitNets(conflict_nets);
            short_violation.setConflitTypes(conflict_types);
            short_violation.setConflitLayer(conflict_layer);
            m_shorts.push_back(short_violation);
        }//end for 
        
        
            
        

        

    }//end drcShorts

    void drcSummary(std::string& file_contents){
        bool enableOut = true;
        std::vector<std::string> violation_types = {
            "Cells","SameNet","Wiring","Antenna","Short","Overlap",
            "Total Violations"
        };
        std::string pattern = "\\s+\\:\\s\\d+";

        auto drc_reports = parseRegex(file_contents,pattern,violation_types);

        //print reports 
        if(enableOut)
            for(auto drc_report : drc_reports)
                std::cout << drc_report << std::endl;
        
    }//end drcSummary 


    std::vector<std::string> parseRegex(std::string& file_contents,std::string& pattern,std::vector<std::string>& violation_types){
        std::vector<std::string> res;
        for(auto violation_type : violation_types){
            std::string current_pattern = violation_type + pattern;
            std::regex tmp_regex(current_pattern); 
            std::smatch match_string;
            std::string tmp_content = file_contents;
            while (std::regex_search (tmp_content,match_string,tmp_regex)) {
                for (auto x:match_string) res.push_back(x);                
                tmp_content = match_string.suffix().str();
            }//end while 
        }//end for 
        return res;
    }//end parseRegex

    std::vector<std::string> parseRegex(std::string& file_contents,std::string& pattern){
        std::vector<std::string> res;
        
        std::string current_pattern = pattern;
        std::regex tmp_regex(current_pattern); 
        std::smatch match_string;
        std::string tmp_content = file_contents;
        while (std::regex_search (tmp_content,match_string,tmp_regex)) {
            for (auto x:match_string) res.push_back(x);                
            tmp_content = match_string.suffix().str();
        }//end while 
        
        return res;
    }//end parseRegex

private:
    std::vector<Short> m_shorts;

    std::string m_geo_file;
    std::string m_score_file;
    std::string m_con_file;

};//end DRCReader class 

};//end


#endif