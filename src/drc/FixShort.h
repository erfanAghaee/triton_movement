// Author: erfan aghaeekiasaraee
// email: erfan.aghaeekiasarae@ucalgary.ca
// Comments: fix short violations

#ifndef _FIXSHORT_H_
#define _FIXSHORT_H_

#include "DRCReader.h"

namespace ucal{

class FixShort{
public:
    FixShort(DRCReader& drc_reader)
        : m_drc_reader(drc_reader)
    {

    }
    ~FixShort(){}

    void run(){
        auto shorts = m_drc_reader.getShorts();
        std::cout << "number of shorts: " << shorts.size() << std::endl;
    }

private:
    DRCReader m_drc_reader;

};//end FixShort

};//end namespace 

#endif 