// Author: erfan aghaeekiasaraee
// email: erfan.aghaeekiasarae@ucalgary.ca
// Comments: read the drc report

#ifndef _FEATURE_EXTRACTION_H_
#define _FEATURE_EXTRACTION_H_


#include <memory>
#include "frBaseTypes.h"
#include "frDesign.h"
using namespace std;
using namespace fr;

namespace ucal{

class FeatureExtraction{
public:
    FeatureExtraction()
    {
        m_region_density_route_box = 0;
        m_region_density_ext_box   = 0;
        m_region_density_drc_box   = 0;

        m_region_density = 0;
        m_design_density = 0;
        m_macro_density    = 0;
        m_macro_tile_neighborhood_overlap = 0;
        m_placement_blockage_tile_neighborhood_overlap = 0;
        m_metal_layer_blockage_tile_neighborhood = 0;
        m_relative_x = 0;
        m_relative_y = 0;
        m_cell_density = 0;
        m_pin_density_tile_neighborhood = 0;
        m_m1_m2_pin_density_tile_neighborhood = 0;
        m_number_of_pio_pins = 0; 
        m_pdist_x = 0;
        m_pdist_y = 0;
        m_local_nets = 0;
        m_global_nets = 0;
        m_hor_passing_net = 0;
        m_ver_passing_net = 0;
        m_max_hor_tracks = 0;
        m_max_ver_tracks = 0;
        m_number_of_cells_route_box= 0;
        m_number_of_cells_drc_box= 0;
        m_number_of_cells_ext_box= 0;
        m_number_of_wires = 0;
        m_number_of_patches = 0;
        m_number_of_vias = 0;
        m_wl_of_worker = 0;
        m_blks = "";
    }
    ~FeatureExtraction(){

    }
    void run();
public:
    // blks information
    std::string m_blks;


    // Routing Density 
    int m_number_of_wires;
    int m_number_of_patches;
    int m_number_of_vias;
    long long m_wl_of_worker;


    // Keep instances information
    vector<frBlockObject*> m_instances; 
    set<string> m_instances_name;
    set<string> m_instances_type;

    // keep the information of boxes that we extract the features
    frBox m_route_box;
    frBox m_ext_box;
    frBox m_drc_box;

    // number of cells in different boxes
    int m_number_of_cells_route_box;
    int m_number_of_cells_drc_box;
    int m_number_of_cells_ext_box;

    //based on global characteristics and utilization factors
    double m_region_density_route_box;
    double m_region_density_ext_box  ;
    double m_region_density_drc_box  ;

    //based on global characteristics and utilization factors
    double m_region_density;
    double m_design_density;

    // narrow channel effect and global characteristics 
    double m_macro_density; // area_of_macros / area_of_design


    // Macro - Tile/ Neighborhood overlap (MT/MNH)
    // routing accessibility and narrow channel effect
    // calculate the tile and neighborhood area overlap with macros 
    double m_macro_tile_neighborhood_overlap;


    // placement blockage - tile/ neighboorhod overlap (PBT/ PBNH)
    // captures utilization factor, calculate the tile and neighborhood
    // area overlap with any routing blockage 
    double m_placement_blockage_tile_neighborhood_overlap;

    // Metal Layers blockages - Tile/ Neighborhood (M, BT/ MiBNH)
    // calculate routing accessibility and routing blockages in different 
    // layers over the tile and the neighborhood area
    double m_metal_layer_blockage_tile_neighborhood;

    // location on x-axis/y-axis (X/Y)
    // Relative location of the tile in the design. 
    // x-axis and y-axis positions of the center of the tile are 
    // obtained are normalized to the range [0 1] for each design
    double m_relative_x;
    double m_relative_y;

    // cell density - Tile/ Neighborhood (CDT/CDNH)
    // capture utilization factor and is the ratio of the total area of the cells
    // within tile/neighborhood to tile/neighborhood area. 
    double m_cell_density;


    // pin density - Tile/ Neighborhood (PDT/PDNH)
    // captures utilization factor calculated as total pin area 
    // with the tile/ neighborhood divided by tile/ neighborhood area. 
    double m_pin_density_tile_neighborhood;


    // M1 and M2 Pin Density - Tile/Neighborhood (M1M2PDT / M1M2PDNH)
    // utilization and routing accessibility factors
    // total pin area within tile/Neighborhood M1 and M2 metal layers
    // divided by tile/Neighborhood area 
    double m_m1_m2_pin_density_tile_neighborhood;


    // Number of PIO pins (IO)
    // captures routing accessibility and special pins and nets factor
    double m_number_of_pio_pins; 


    // Pin distribution on x-axis/y-axis (PdistX/ PdistY)
    // standard deviation of the x and y locations of a pin in the tile and 
    // captures utilization factor
    double m_pdist_x;
    double m_pdist_y;


    // local net in tile/ Neighborhood (LNetT / LNetNH )
    // calculate total numner of nets that have at least 
    // two pins in a tile/neighborhood and captures connectivity factors
    double m_local_nets;


    // global net in tile/ Neighborhood (GNet/ GNetNH)
    // global net is calculated as the number of nets that have at least 
    // one pin in a tile and at least one pin outside the tile
    double m_global_nets;

    // Horizontally/Vertically passing Net (PNetH/ PNetV)
    // captures connectivity and effects of narrow channels factors 
    // by estimating the number of nets passing the tile horizontally/vertically. 
    // each column/row of constructed tile grid, the number of nets 
    // having at least one pin on both sides of the column/row is calculated 
    // and divided by the number of the tile in the column/row.
    double m_hor_passing_net;
    double m_ver_passing_net;

    // Maximum Horizontal/vertical tracks
    // estimations of maximum number of horizontal/vertical tracks using the 
    // line scan algorithm
    double m_max_hor_tracks;
    double m_max_ver_tracks;

};//end FeatureExtraction class 


};//end


#endif