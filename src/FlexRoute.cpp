/* Authors: Lutong Wang and Bangqi Xu */
/*
 * Copyright (c) 2019, The Regents of the University of California
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include "global.h"
#include "FlexRoute.h"
#include "io/io.h"
#include "pa/FlexPA.h"
#include "ta/FlexTA.h"
#include "dr/FlexDR.h"
#include "gc/FlexGC.h"
#include "rp/FlexRP.h"
#include "drc/DRCReader.h"
#include "drc/FixShort.h"
#include "drc/FeatureExtraction.h"

using namespace std;
using namespace fr;

void FlexRoute::init() {
  io::Parser parser(getDesign());
  parser.readLefDef();
  parser.readGuide(); 
  parser.postProcess();

  bool enableOutput = false;
  if(enableOutput){
    std::cout << "present guides of nets after parser post process ...\n";
    auto guides = getDesign()->getTopBlock()->getGuides();
    for(auto guide : guides){
      const auto& net = guide->getNet();
      std::cout << "net name: " << net->getName() << std::endl;
      frBox rect_box;
      guide->getBBox(rect_box);
      std::cout << "net name: " << rect_box.lowerLeft().x() / 2000.0 << ", "
                                << rect_box.lowerLeft().y() / 2000.0 << ", "
                                << rect_box.upperRight().x() / 2000.0 << ", "
                                << rect_box.upperRight().y() / 2000.0 << "\n";
    }
    
  }//end if enableOutput

  
  FlexPA pa(getDesign());
  pa.main();
  parser.postProcessGuide();
}

void FlexRoute::prep() {
  FlexRP rp(getDesign(), getDesign()->getTech());
  rp.main();
}

void FlexRoute::ta() {
  FlexTA ta(getDesign());
  ta.main();
  io::Writer writer(getDesign());
  writer.writeFromTA();
}

void FlexRoute::dr() {
  FlexDR dr(getDesign());
  dr.main();
}

void FlexRoute::endFR() {
  io::Writer writer(getDesign());
  writer.writeFromDR();
}

void FlexRoute::drcReader(){
  ucal::DRCReader drc_reader;
  drc_reader.run();
  auto shorts = drc_reader.getShorts();
  std::cout << "Number of shorts: " << shorts.size() << std::endl;
  ucal::FixShort fix_short(drc_reader);
  fix_short.run();
  FlexDR dr(getDesign());
  dr.routeNet();
}//end drcReader

void FlexRoute::fixDRC(){
  // ucal::FixShort fix_short();
  // fix_short.run();
}
void FlexRoute::featureExtraction(){
  // ucal::FeatureExtraction feature_extraction(getDesign());
  
  // feature_extraction.run();
  FlexDR dr(getDesign());
  dr.mainFeatureExtraction();
  // dr.main();
  // dr.featureExtraction();
}//end featureExtraction

void FlexRoute::movement(){
  // ucal::FeatureExtraction feature_extraction(getDesign());
  bool enableOutput = true;
  // feature_extraction.run();
  auto& instances = getDesign()->getTopBlock()->getInsts();
  for(auto& inst : instances){
    if(inst->getName() == "inst8879"){
      frOrient new_orient = frcMY;
      inst->setOrient(new_orient);

      

      // update guide: 
      auto& nets = inst->getNets();
      for(auto& net : nets){
        auto& guides = net->getGuides();
        for(auto& guide:guides){
          frBox guide_box;
          guide->getBBox(guide_box);
          if(enableOutput){
            std::cout << "guide box: " <<guide_box.lowerLeft().x()   << ", "
                                       <<guide_box.lowerLeft().y()   << ", "
                                       <<guide_box.upperRight().x()  << ", "
                                       <<guide_box.upperRight().y()  << "\n";

          }
        }//end for 
        

        for(auto& inst_terms : net->getInstTerms()){
          auto accesspoints = inst_terms->getAccessPoints();
          accesspoints.clear();
        }
        
      }

    }
  }

  // dr.main();
  // dr.featureExtraction();
}//end featureExtraction

int FlexRoute::main() {
  io::Parser parser(getDesign());
  // iteration 0
  init();
  prep();
  ta();
  // dr(); 
  // movement();
  // // iteration 1
  // parser.readGuide();
  // parser.postProcess();
  // FlexPA pa(getDesign());
  // pa.main();
  // parser.postProcessGuide();
  // prep();
  // ta();
  // dr(); 
  
  
  // check the drc report 
  // drcReader();

  featureExtraction();

  endFR();

  return 0;
}

